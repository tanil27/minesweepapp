import React from 'react';
import { useForm } from 'react-hook-form';

import "./form.css";

const GameFormContainer = ({onSubmit}) => {
  const { register, handleSubmit, watch, formState: { errors, touched } } = useForm({mode: 'onBlur'});
  const watchFields = watch(["brdWidth", "brdHeight"]);

  const onSubmitHandler = values => {
    
    console.log("preVal: ", values);
    for (let key in values) {
      values[key] = parseInt(values[key])
    }
    console.log("submit: ", values);
    onSubmit({ values });
  
    return false;
  }
  
console.log(errors);

  return (
    <div id='formAddGame' className='formAddGameWrapper'>
      <form name='addGameForm' id='addGameFormID' onSubmit={handleSubmit(onSubmitHandler)}>
        <label className={`${errors?.playerCount ? "inValid" : ""}`}>Player Count: <input type="number" {...register("playerCount", { required: true, min: 1 })}/></label>
        <label className={`${errors?.playerLives ? "inValid" : ""}`}>Player Lives: <input type="number" {...register("playerLives", { required: true, min: 1 })}/></label>
        <label className={`${errors?.brdWidth ? "inValid" : ""}`}>Board  Width: <input type="number" {...register("brdWidth", { required: true, min: 10 })}/></label>
        <label className={`${errors?.brdHeight ? "inValid" : ""}`}>Board Height: <input type="number" {...register("brdHeight", { required: true, min: 10 })}/></label>
        <label className={`${errors?.mineAmount ? "inValid" : ""}`}>Mine  Amount: <input type="number" {...register("mineAmount", { 
          validate: {
            isMineCountInBounds: v =>  v <= (parseInt(watchFields[0]) * parseInt(watchFields[1]) * 0.7) || 'Mine count exceeds boards limit'
          },
          required: true, min: 1 })}/></label>
        <input type='submit' value='Add Game' className='submitBtn submitAddGameForm' id='submitAddGameFormBtn'/>
      </form>
    </div>
  );
}

export default GameFormContainer