import React, {useState} from 'react';

const InputComponent = ({label, validator, disabled = false, type='number', classNames='', name, isRequired=true}) =>
{
  const [isValid, setValidity] = useState(true);

  console.log(`${label} : `, isValid);
  return(
    <label>{label}
      <input
        onBlur={(e) => setValidity(validator ? validator(e.target.value) : true)}
        disabled={disabled}
        type={type}
        name={name}
        required={ isRequired }
        className={`${classNames} ${isValid ? '' : 'invalid'}`}
      />
    </label>
  )
}

export default InputComponent;