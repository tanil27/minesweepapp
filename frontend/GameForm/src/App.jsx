import React, {useState} from 'react';
import ReactDOM from 'react-dom';

import './index.css';
import GameFormContainer from "./container/GameFormContainer";

const App = () => {
  const [formState, setFormState] = useState({});
  console.log(formState);
  return (<>
      <GameFormContainer
        onSubmit={(inputValues) => setFormState(inputValues)}
      />
      <div>
        Values:<br/>
        {formState?.values && Object.values(formState.values).map((val, i)=><div key={i}>{val}</div>)}
      </div>
    </>
  );
}
ReactDOM.render(<App/>, document.getElementById('app'));