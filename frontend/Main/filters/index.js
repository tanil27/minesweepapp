import socket from "../js/network/socket/ws";
import {onMessage, onOpen} from "../js/app/app";

function saveText(data) {
  var jsonStr = JSON.stringify(data);
  var filename = data.action;

  console.log("------SAVING FILE: '", filename ,"' -----------");
  var a = document.createElement('a');
  a.setAttribute('href', 'data:text/json;charset=utf-u,'+encodeURIComponent(jsonStr));
  a.setAttribute('download', playerDataObj.getPlayer()+"_"+filename+".json");
  a.click();
}


/* global socket, playerDataObj */


export function createFilteredSocket(e) {

  console.error("SOCKET ERROR: ", e, "\n ... going to revert to filters");
  //use filters
  function QueryableWorker(url) {
    var instance = this,
      worker = new Worker(url),
      listeners = {};

    this.type = "filter";

    this.connect = function () {
      this.send(JSON.stringify({action: "connect"}));
    };

    this.onConnect = function (e) {
      onMessage(e[0]);
    };

    this.postMessage = function (message) {
      worker.postMessage(message);
    };

    this.terminate = function () {
      worker.terminate();
    };

    this.addEventListener = function (name, listener) {
      listeners[name] = listener;
    };

    this.removeListener = function (name) {
      delete listeners[name];
    };

    /*
     This functions takes at least one argument, the method name we want to query.
     Then we can pass in the arguments that the method needs.
     */
    this.send = function (data) {
      var messageJSON = JSON.parse(data);
      worker.postMessage({
        'message': messageJSON,
        'session': playerDataObj ? playerDataObj.getPlayer() : null
      });
    };

    worker.onmessage = function (event) {
      if (event.data instanceof Object &&
        event.data.hasOwnProperty('queryMethodListener') &&
        event.data.hasOwnProperty('queryMethodArguments') &&
        listeners[event.data.queryMethodListener]) {
        listeners[event.data.queryMethodListener].apply(instance, event.data.queryMethodArguments);
      } else {
        instance.onConnect.call(instance, event.data.queryMethodArguments);
      }
    };
    this.connect.call(instance);
  }
  socket.ws = new QueryableWorker('filters/fauxServer.js');
  socket.ws.addEventListener("message", function (e) {
    onMessage(e);
  });
  socket.ws.addEventListener("open", function (e) {
    onMessage(e);
    onOpen(e);
  });
}
