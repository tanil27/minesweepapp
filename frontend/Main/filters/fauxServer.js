var connected = false;
var timeSync;

// system functions
function OnOpen() {
    bot = new Bot();
    loadFilterJson("json/connected.json");
    //timeSync = setInterval(sendTimeSync,10000);
}

function dateAdd(date, interval, units) {
    var ret = new Date(date); //don't change original date
    var checkRollover = function() { if(ret.getDate() != date.getDate()) ret.setDate(0);};
    switch(interval.toLowerCase()) {
        case 'year'   :  ret.setFullYear(ret.getFullYear() + units); checkRollover();  break;
        case 'quarter':  ret.setMonth(ret.getMonth() + 3*units); checkRollover();  break;
        case 'month'  :  ret.setMonth(ret.getMonth() + units); checkRollover();  break;
        case 'week'   :  ret.setDate(ret.getDate() + 7*units);  break;
        case 'day'    :  ret.setDate(ret.getDate() + units);  break;
        case 'hour'   :  ret.setTime(ret.getTime() + units*3600000);  break;
        case 'minute' :  ret.setTime(ret.getTime() + units*60000);  break;
        case 'second' :  ret.setTime(ret.getTime() + units*1000);  break;
        case 'millisecond' :  ret.setTime(ret.getTime() + units);  break;
        default       :  ret = undefined;  break;
    }
    return ret;
}

function sendTimeSync() {
    var timeNow = new Date();
    var timeSyncObj = {
        action: "timeSync",
        time: timeNow
    };
    var response = JSON.stringify(timeSyncObj);
    var event = {data: response};
    sendToSession(event);
}

function reply() {
    if (!connected) {
        connected = true;
        postMessage({'queryMethodListener': "open", 'queryMethodArguments': Array.prototype.slice.call(arguments, 1)});
    } else {
        if (arguments.length < 1) {
            throw new TypeError('reply - not enough arguments');
            return;
        }
        postMessage({
            'queryMethodListener': arguments[0],
            'queryMethodArguments': Array.prototype.slice.call(arguments, 1)
        });
    }
}

onmessage = function (oEvent) {
    if (oEvent.data instanceof Object && oEvent.data.hasOwnProperty('message') && oEvent.data.hasOwnProperty('session')) {
        //console.log("MESSAGE CLIENT -> FAUX SERVER: ", oEvent.data);
        var messageJSON = oEvent.data.message;
        var sessionJSON = oEvent.data.session;
        if (queryableFunctions[messageJSON.action] && sessionJSON) {
            queryableFunctions[messageJSON.action].apply(self, [messageJSON, sessionJSON]);
        } else if (!sessionJSON && !connected) {
            console.log("ERROR: need to connect session");
            OnOpen();
        }
    } else {
        console.log("ERRROR: fauxServer ONmessage");
    }
};

function polyFill() {
    if (!Array.prototype.includes) {
        Object.defineProperty(Array.prototype, 'includes', {
            value: function (searchElement, fromIndex) {

                // 1. Let O be ? ToObject(this value).
                if (this == null) {
                    throw new TypeError('"this" is null or not defined');
                }

                var o = Object(this);

                // 2. Let len be ? ToLength(? Get(O, "length")).
                var len = o.length >>> 0;

                // 3. If len is 0, return false.
                if (len === 0) {
                    return false;
                }

                // 4. Let n be ? ToInteger(fromIndex).
                //    (If fromIndex is undefined, this step produces the value 0.)
                var n = fromIndex | 0;

                // 5. If n ≥ 0, then
                //  a. Let k be n.
                // 6. Else n < 0,
                //  a. Let k be len + n.
                //  b. If k < 0, let k be 0.
                var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

                function sameValueZero(x, y) {
                    return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
                }

                // 7. Repeat, while k < len
                while (k < len) {
                    // a. Let elementK be the result of ? Get(O, ! ToString(k)).
                    // b. If SameValueZero(searchElement, elementK) is true, return true.
                    // c. Increase k by 1.
                    if (sameValueZero(o[k], searchElement)) {
                        return true;
                    }
                    k++;
                }

                // 8. Return false
                return false;
            }
        });
    }
}

var FIELD_STATES = {
    "CHECKED": -3,
    "FLAGGED": -2,
    "MINE": -1,
    "EMPTY": 0,
    "NUMBER": 1
};
debug = false;
var loadedGame = {};
gameIndex = 0;

var players = {};
playerIndex = 0;

function sendToSession(data) {
    if(JSON.parse(data.data).action != "timeSync") {
        //console.log("MESSAGE: FAUX SERVER -> CLIENT ... ", JSON.parse(data.data));
    }
    reply("message", data);
}

function loadFilterJson(filename, callback, forceCallBack) {
    console.log("load JSON: ", filename);
    function loadJSON(filename, callback) {
        var xobj = new XMLHttpRequest();
        xobj.overrideMimeType("application/json");
        xobj.open('GET', filename, true); // Replace 'my_data' with the path to your file
        xobj.onreadystatechange = function () {
            if (xobj.readyState == 4 && xobj.status == "200") {
                // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
                callback(xobj.responseText);
            }
        };
        xobj.send(null);
    }


    loadJSON(filename, function (response) {
        var js = JSON.parse(response);

        if(js.action == "connected"){
            js["time"] = new Date();
        }
        if(js.action == "newGame") {
            js.game["gameStart"] = dateAdd(new Date(), 'millisecond', delay);
        }
        if(js.action == "gamesListUpdate") {
            var games = js["games"];

            for(var i = 0; i < games.length; i++) {
                var game = games[i];
                if(game.phase == "pregame_phase") {
                    var timeDelay = dateAdd(new Date(), 'millisecond', delay);
                    game["gameStart"] = timeDelay;
                    clearTimeout(GLpregamephaseTimeOut);
                    GLpregamephaseTimeOut = setTimeout(function () {
                        //send the ingame phase
                        sendToSession({data: JSON.stringify({
                            "action": "phaseUpdate",
                            "gameID": game.id,
                            "phase": "ingame_phase",
                            "gameStart": timeDelay,
                            "locked": game.locked
                        })});
                    }, delay);
                }
            }
        }

        response = JSON.stringify(js);
        var event = {data: response};

        if(forceCallBack) {
            if (callback) {
                callback(event);
            }
        }
        sendToSession(event);
        if (!forceCallBack && callback) {
            callback(event);
        }
    });
}

var gameListCount = 0;

var activeGame = null;

var GLpregamephaseTimeOut;
var pregamephaseTimeOut;

var gameIDidx = 10;
var minelistUpdateIdx = 1;
var boardUpdateIdx = 0;
var phaseUpdate = 0;
var mouseClickCount = 0;

var secDelay = 5;
var delay = secDelay*1000;

var botInterval = null;
var randGameCount = 1000;


var Bot = function() {
    this.idCount = gameIDidx;
    this.botID = "nGtAgjPwgRot7ekuZwKu1hPiTItdM0Y4ERtbFFRx";
};
Bot.prototype = {
    runNewGame: function () {
        clearTimeout(botInterval);
        if (randGameCount > 0) {
            var randDelay = Math.round(Math.random() * (20)) + 5;
            botInterval = setTimeout(function () {

                var randLocked = Math.random() <= 0.34;
                var randDig = Math.random();
                var randPhase = "pregame_phase";
                var gameStartTime = dateAdd(new Date(), 'millisecond', delay);
                if(randDig < 0.25) {
                    randPhase = "win_phase";
                    gameStartTime = "2017-11-26T20:26:03.354";
                } else if (randDig < 0.5) {
                    randPhase = "ingame_phase";
                    gameStartTime = "2017-11-26T20:26:03.354";
                } else {
                    console.log("TTT: SETTING PHASE STATE: ", this.idCount);
                    var idCount = this.idCount;
                    setTimeout(function () {
                        console.log("TTT: TIMING ON PHASE STATE: ", idCount);
                        sendToSession({
                            data: JSON.stringify({
                                "action": "phaseUpdate",
                                "gameID": idCount,
                                "phase": "ingame_phase",
                                "gameStart": "2017-11-26T20:26:03.354",
                                "locked": randLocked
                            })
                        })
                    }.bind(this), delay)
                }

                var randPlayerCount = Math.round(Math.random() * 50) + 2;
                var randBrdD =  Math.round(Math.random() * 10000) + 10;

                sendToSession({
                    data: JSON.stringify({
                        "action": "newGame",
                        "game": {
                            "id": this.idCount,
                            "timeCreated": "2017-11-26T20:25:58.354",
                            "gameStart": gameStartTime,
                            "locked": randLocked,
                            "config": {
                                "playerAmount": randPlayerCount,
                                "playerLivesAmt": randPlayerCount,
                                "customMineAmt": randBrdD,
                                "brdWidth": randBrdD,
                                "brdHeight": randBrdD
                            },
                            "host": this.botID,
                            "phase": randPhase
                        }
                    })
                });
                this.idCount++;
                randGameCount--;
                this.runNewGame();
            }.bind(this), randDelay);
        }
        console.log("GAMELIST BOT COUNT: ", 1000 - randGameCount);
    }
};


var queryableFunctions = {

    getGamesList: function (id) {
        loadFilterJson("json/gamesListUpdate.json");
        bot.runNewGame();
    },

    newGame: function () {
        this.queryableFunctions.joinGame({gameID: gameIDidx});
        gameIDidx++;
        phaseUpdate = 0;
    },

    joinGame: function (msgJS) {
        var gameDir = "json/game_" + msgJS.gameID;
        var phase = null;
        loadFilterJson(gameDir + "/newgame.json", function (e) {
            var serverMessage = JSON.parse(e.data);

            //wrapped in callback to avoid JS sync issues
            loadFilterJson(gameDir + "/joinGame.json", function (e) {
                if(serverMessage.game.phase == "pregame_phase") {
                    this.queryableFunctions.gamePhaseUpdate(gameDir);
                    this.queryableFunctions.minelistUpdate(gameDir);
                }
            });
            loadFilterJson(gameDir + "/joinGameBoardsUpdate ("+boardUpdateIdx+").json", function (e) {
                boardUpdateIdx++;
                setTimeout(function () {
                    loadFilterJson(gameDir + "/joinGameBoardsUpdate ("+boardUpdateIdx+").json");
                }, 2000);
            });

        });
    },

    gamePhaseUpdate: function (gameDir) {
        clearTimeout(pregamephaseTimeOut);
        pregamephaseTimeOut = setTimeout(function () {
            loadFilterJson(gameDir + "/phaseUpdate ("+phaseUpdate+").json", function (e) {
                var serverMessage = JSON.parse(e.data);
                if(serverMessage.phase != "ingame_phase") {
                    this.queryableFunctions.gamePhaseUpdate(gameDir);
                    if(serverMessage.phase == "pregame_phase") {
                        this.queryableFunctions.minelistUpdate(gameDir);
                    }
                }
            });
            phaseUpdate++;
            if(phaseUpdate == 4) {
                phaseUpdate = 0;
            }
        }, delay);
    },

    mouseClick: function (msgJS) {
        /*
        var gameDir = "json/game_" + msgJS.gameID;
        loadFilterJson(gameDir + "/mouseClicked (" + mouseClickCount + ").json", function (e) {
            var serverMessage = JSON.parse(e.data);
            if(serverMessage.gameOver) {
                //send win phase update
                this.queryableFunctions.gamePhaseUpdate(gameDir);
                mouseClickCount = 0;
            }
            mouseClickCount++;
        });
        */
        var gameOver = false;
        if(mouseClickCount == 6) {
            gameOver = true;
            //send win phase update
            this.queryableFunctions.gamePhaseUpdate("json/game_" + 10);
            mouseClickCount = 0;
        }
        var js = {
            "action": "mouseClicked",
            "clickedBy": {
                "id": "EHechYemZyCcbfJc_eLb3hEnH2qbDz1r5rDbilnZ"
            },
            "coord": {
                "x": msgJS.x,
                "y": msgJS.y
            },
            "mouseBtn": msgJS.mouseButton,
            "gameOver": gameOver
        };
        mouseClickCount++;
        response = JSON.stringify(js);
        var event = {data: response};
        sendToSession(event);
    },

    minelistUpdate: function (gameDir) {
        loadFilterJson(gameDir + "/minelistUpdate ("+minelistUpdateIdx+").json");
        //minelistUpdateIdx++;
    },

    restartGame: function () {
        mouseClickCount = 0;
        loadFilterJson("json/restartGame.json");
    },

    leaveGame: function (msgJS) {
        mouseClickCount = 0;
        gameListCount = 1;
        clearTimeout(pregamephaseTimeOut);
        var gameDir = "json/game_" + msgJS.gameID;
        loadFilterJson(gameDir + "/leaveGame.json");
    }
};