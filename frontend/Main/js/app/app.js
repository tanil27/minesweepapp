import GlobalParams from "../store/GlobalParams";
import {addGameFormToDom} from "../view/addGameForm.jsx";
import {getGamesList} from "./api";
import PlayerData from "../model/PlayerData";
import {addGamesListToDom} from "../view/gameslist.html";
import {addInGameStatsToDom} from "../view/inGameStats.html";
import {addGameBoardToDom} from "../view/gameboard.html";
import GameConfig from "../model/GameConfig";
import GameRoom from "../model/GameRoom";


export function renderPage() {
    addGameFormToDom();
}

export function onOpen(e) {
    console.log("OPENED A CONNECTION");
    getGamesList();
    pingServer();
}

function  pingServer() {

}

export function getTimeRemaining(time) {
    var t = Date.parse(time) - (Date.now() + GlobalParams.appTime);

    var seconds = Math.floor( (t/1000) % 60 );
    var minutes = Math.floor( (t/1000/60) % 60 );
    var hours = Math.floor( (t/(1000*60*60)) % 24 );
    var days = Math.floor( t/(1000*60*60*24) );

    return {
        'total': t,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds,
        'toString': function () {
            return days + "d :" + hours + "hrs :" + minutes + "m :" + seconds + "s";
        }
    };
}

function _syncLocalDateTime(serverTime) {
    console.log(serverTime);
    const localTime = new Date();
    const adjustedTime = serverTime - localTime;
    GlobalParams.appTime = adjustedTime;
}

function handleLobbyRender() {
    if(GlobalParams.inLobby) {
        console.log("----------------GAME LIST UPDATE--------------");
        addGamesListToDom(GlobalParams.gameRoomsList);
        clearInterval(GlobalParams.lobbyRenderer);
        GlobalParams.lobbyRenderer = setInterval(function(){
            addGamesListToDom(GlobalParams.gameRoomsList);
        },250);
        setTimeout(function (){
            
        },getTimeRemaining(GlobalParams.longestSyncTime));
    }
}
function setLongestSyncTime(gameRoom) {
    if(new Date(gameRoom.getGameStart()).getTime() > GlobalParams.longestSyncTime) {
        GlobalParams.longestSyncTime = Date.parse(gameRoom.getGameStart());
        console.log(GlobalParams.longestSyncTime);
    }
}

function createNewGame(configJS, gameJS) {    
    const config = new GameConfig(configJS.playerAmount, configJS.playerLivesAmt, configJS.customMineAmt, configJS.brdWidth, configJS.brdHeight);
    const gameRoom = new GameRoom(gameJS.id, gameJS.host, config, gameJS.timeCreated);
    gameRoom.setGameLocked(gameJS.locked);
    gameRoom.setGameStart(gameJS.gameStart);
    gameRoom.setPhase(gameJS.phase);
    return gameRoom;
}

function handleConnectionMessage(serverMessage){
    console.log("----------------CONNECTED--------------");
    GlobalParams.playerDataObj = new PlayerData(serverMessage.player);

    const serverTime = new Date(serverMessage.time);
    _syncLocalDateTime(serverTime);
}
function handleGamesListUpdateMessage(serverMessage) {
    const gamesJSarr = serverMessage.games;
    
    for(let i = 0; i < gamesJSarr.length; i++) {
        const gameJS = gamesJSarr[i];
        const configJS = gameJS.config;
        const newGameRoom = createNewGame(configJS, gameJS);
        newGameRoom.setBoardCount(gameJS.boardCount);
        setLongestSyncTime(newGameRoom);
        
        /*
        for(var j = 0; j < gameJS.boards.length; j++) {
            var boardJS = gameJS.boards[j];
            newGameRoom.addPlayer(boardJS.id, boardJS.player.id);
        }*/
        GlobalParams.gameRoomsList.addGame(newGameRoom);
    }

    if(GlobalParams.inLobby) {
        console.log("----------------GAME LIST UPDATE--------------");
        addGamesListToDom(GlobalParams.gameRoomsList);
        clearInterval(GlobalParams.lobbyRenderer);
        GlobalParams.lobbyRenderer = setInterval(function(){
            addGamesListToDom(GlobalParams.gameRoomsList);
        },250);
    }
}
function handleNewGameMessage(serverMessage) {
    console.log("----------------NEW GAME--------------");

    let gameRoom;
    const gameJS = serverMessage.game;
    if(!GlobalParams.gameRoomsList.getGame(gameJS.id)) {
        console.log("new game not in list");
        const configJS = gameJS.config;
        gameRoom = createNewGame(configJS, gameJS);
        
        setLongestSyncTime(gameRoom);

        GlobalParams.gameRoomsList.addGame(gameRoom);
    } else {
        gameRoom = GlobalParams.gameRoomsList.getGame(gameJS.id);
    }
}
function handleJoinGameMessage(serverMessage){
    console.log("----------------JOIN GAME--------------");

    let gameRoom;
    if(GlobalParams.gameRoomsList.getGame(serverMessage.gameID)) {
        GlobalParams.inLobby = false;
        clearInterval(GlobalParams.lobbyRenderer);
        gameRoom = GlobalParams.gameRoomsList.getGame(serverMessage.gameID);
        //only when joined
        gameRoom.setupGame();
        if(gameRoom.isInGamePhase()) {
            gameRoom.setJoinedLate(true);
        }
        const me = GlobalParams.playerDataObj;
        const boardsJS = serverMessage.boards;
        for(let i = 0 ; i < boardsJS.length; i++) {
            var boardJS = serverMessage.boards[i];
            var playerID = boardJS.player.id;
            if(boardJS.player.id == me.getPlayer()) {
                playerID = me;
            }
            gameRoom.addPlayer(boardJS.id, playerID);
        }
    }

    GlobalParams.activeGame = gameRoom;
    addGameBoardToDom(GlobalParams.activeGame);
    addInGameStatsToDom(GlobalParams.activeGame);
}
function handleJoinGameBoardsUpdateMessage(serverMessage) {
    var gameRoom;
    if(GlobalParams.gameRoomsList.getGame(serverMessage.gameID)) {
        gameRoom = GlobalParams.gameRoomsList.getGame(serverMessage.gameID);
    }
    if(GlobalParams.activeGame){
        if(gameRoom.getId() == GlobalParams.activeGame.getId()) {
            var me = GlobalParams.playerDataObj;
            var boardJS = serverMessage.newBoard;
            if(boardJS.player.id != me.getPlayer()) {
                gameRoom.addPlayer(boardJS.id, boardJS.player.id);
            }
        }
    }
    gameRoom.setBoardCount(gameRoom.getBoardCount() + 1);
    if(!GlobalParams.inLobby) {
        addGameBoardToDom(GlobalParams.activeGame);
        addInGameStatsToDom(GlobalParams.activeGame);
    }
}

function handleMineListUpdate(serverMessage) {
    var gameRoom;
    if(GlobalParams.gameRoomsList.getGame(serverMessage.gameID)) {
        var gameRoom = GlobalParams.gameRoomsList.getGame(serverMessage.gameID);
    }

    if(gameRoom.getId() == GlobalParams.activeGame.getId()) {
        var minelistJS = serverMessage.minelist;
        var tempML = [];
        for(var i = 0; i < minelistJS.length; i++) {
            var mineJs = minelistJS[i];
            var cellData = gameRoom.getFieldGrid().getCell(mineJs.x, mineJs.y);
            cellData.setMine();
            tempML.push(cellData);
        }
        console.log("ML: ", tempML);
        gameRoom.initBoardState(tempML);
    }
}

function handlePhaseUpdateMessage(serverMessage){
    if(GlobalParams.gameRoomsList.getGame(serverMessage.gameID)) {
        var gameRoom = GlobalParams.gameRoomsList.getGame(serverMessage.gameID);
        gameRoom.setPhase(serverMessage.phase);
    } else {
        console.error("ERR: ", {code: 101, des:"Game Not Found"});
    }
    
    if(GlobalParams.activeGame && GlobalParams.activeGame.getId() == serverMessage.gameID) {
        if(gameRoom.isPreGamePhase()) {
            console.log("----------------PRE GAME--------------");
            gameRoom.reset();
            gameRoom.setJoinedLate(false);
            gameRoom.setGameStart(serverMessage.gameStart);
                        
            setLongestSyncTime(gameRoom);
        } else 
        if(gameRoom.isInGamePhase()) {
            console.log("----------------GAME STARTED--------------");

        } else
        if(gameRoom.isWinGamePhase()) {
            console.log("----------------GAME OVER--------------");
            //reveal the scores
            gameRoom.gameOver();
        }
    }

    if(GlobalParams.activeGame && !GlobalParams.activeGame.getJoinedLate()) {
        addGameBoardToDom(GlobalParams.activeGame);
        addInGameStatsToDom(GlobalParams.activeGame);
    }
}
function handleLeaveGameMessage(serverMessage){
    console.log("----------------LEAVE GAME--------------");
    
    if(GlobalParams.gameRoomsList.getGame(serverMessage.gameID)) {
        var gameRoom = GlobalParams.gameRoomsList.getGame(serverMessage.gameID);
        
        if(GlobalParams.activeGame && gameRoom.getId() == GlobalParams.activeGame.getId()) {
            var leftBoardPlayer = serverMessage.leftPlayerID;
            var leftPlayerData = gameRoom.getFieldGrid().getPlayer(leftBoardPlayer).getPlayer().getPlayer();
            gameRoom.getFieldGrid().removePlayer(leftPlayerData);        
            addInGameStatsToDom(GlobalParams.activeGame);
        }
        gameRoom.setBoardCount(gameRoom.getBoardCount() - 1);
        if(gameRoom.getBoardCount() == 0) {
            //game empty so remove from list
            GlobalParams.gameRoomsList.removeGame(gameRoom);
        } else if(gameRoom.getBoardCount() < gameRoom.getConfig().getPlayerAmount()) {
            gameRoom.setGameLocked(false);
            gameRoom.setHost(serverMessage.host);
        }
    }
    
    if(GlobalParams.inLobby) {
        console.log("----------------GAME LIST UPDATE--------------");
        addGamesListToDom(GlobalParams.gameRoomsList);
        clearInterval(GlobalParams.lobbyRenderer);
        GlobalParams.lobbyRenderer = setInterval(function(){
            addGamesListToDom(GlobalParams.gameRoomsList);
        },250);
    }
}
function handleMouseClickedMessage(serverMessage){
    console.log("----------------MOUSE CLICKED--------------");

    if (GlobalParams.activeGame.getJoinedLate())
    {
        return;
    }

    var affectedPlayer = serverMessage.clickedBy.id;
    var cboard = GlobalParams.activeGame.getFieldGrid().getBoard(affectedPlayer);

    GlobalParams.activeGame.mouseUpEventHandler(serverMessage.coord.x, serverMessage.coord.y, serverMessage.mouseBtn, cboard, serverMessage.affectedCells);


    if(serverMessage.gameOver) {
        //handle win stylings
        console.log("========GAMEOVER=======");
        GlobalParams.activeGame.doGameOver();
        
    }
    
    if(!GlobalParams.activeGame.getJoinedLate()) {
        addGameBoardToDom(GlobalParams.activeGame);
        addInGameStatsToDom(GlobalParams.activeGame);
    }
}

function handleRestartGameMessage(serverMessage){
    console.log("----------------RESTART GAME--------------");
    addGameBoardToDom(GlobalParams.activeGame);
    addInGameStatsToDom(GlobalParams.activeGame);
}
function handleTimeSyncMessage(serverMessage){
    var serverTime = new Date(serverMessage.time);
    _syncLocalDateTime(serverTime);
    if(GlobalParams.inLobby) {
        addGamesListToDom(GlobalParams.gameRoomsList);
    }
}


export function onMessage(e) {
    var serverMessage = JSON.parse(e.data).message;

    if(serverMessage.appTime) {
        _syncLocalDateTime(serverMessage.appTime);
    }
    console.log("==================IN JSON==================");
    console.log(serverMessage);

    if(serverMessage.action === "connected") {
        handleConnectionMessage(serverMessage);
    }
    else if(serverMessage.action === "newGame") {
        handleNewGameMessage(serverMessage);
    }
    else if(serverMessage.action === "joinGame") {
        handleJoinGameMessage(serverMessage);
    }
    else if(serverMessage.action === "joinGameBoardsUpdate") {
        handleJoinGameBoardsUpdateMessage(serverMessage);
    }
    else if(serverMessage.action === "minelistUpdate") {
        handleMineListUpdate(serverMessage);
    }
    else if(serverMessage.action === "phaseUpdate") {
        handlePhaseUpdateMessage(serverMessage);
    }
    else if(serverMessage.action === "leaveGame") {
        handleLeaveGameMessage(serverMessage);
    }
    else if(serverMessage.action === "gamesListUpdate") {
        handleGamesListUpdateMessage(serverMessage);
    }
    else if(serverMessage.action === "mouseClicked") {
        handleMouseClickedMessage(serverMessage);
    }
    else if(serverMessage.action === "restartGame") {
        handleRestartGameMessage(serverMessage);
    }
    else if(serverMessage.action === "timeSync") {
        handleTimeSyncMessage(serverMessage);
    }

    console.log("===========================================");

    //saveText(serverMessage);
}

