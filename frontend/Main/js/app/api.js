import socket from "../network/socket/ws";
import GlobalParams from "../store/GlobalParams";

export function newGame(playerCount, playerLives, mineAmount, brdWidth, brdHeight) {
    var newGameAction = {
        action: "newGame",
        gridDimension: [brdWidth,brdHeight],
        mineCount: mineAmount,
        playerCount: playerCount,
        playerLives: playerLives
    };
    socket.send(newGameAction);
}

export function joinGame(id) {
    var joinGameAction = {
        action: "joinGame",
        gameID: id
    };
    socket.send(joinGameAction);
}

export function mouseClicked(x, y, mouseButton, gameID) {
    var mouseClickedAction = {
        action: "mouseClick",
        x: x,
        y: y, 
        mouseButton: mouseButton, 
        gameID: gameID
    };
    socket.send(mouseClickedAction);
}

export function leaveGame(gameID) {
    GlobalParams.activeGame.getFieldGrid().removePlayer(GlobalParams.playerDataObj.getPlayer());
    GlobalParams.activeGame = null;
    GlobalParams.inLobby = true;


    var leaveGameAction = {
        action: "leaveGame",
        gameID: gameID
    };
    socket.send(leaveGameAction);
}


export function getGamesList() {
    var getGamesAction = {
        action: "getGamesList"
    };
    socket.send(getGamesAction);
}

export function restartGame(id) {
    var restartGameAction = {
        action: "restartGame",
        gameID: id
    };
    socket.send(restartGameAction);
}
