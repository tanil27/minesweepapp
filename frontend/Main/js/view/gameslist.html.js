import {replaceHtml} from "../utils/DomUtils";
import {destroyGameForm} from "./addGameForm.jsx";
import {joinGame} from "../app/api";
import {getTimeRemaining} from "../app/app";

function loadGamesListHtml(container, data) {
    var t0 = performance.now();
    var domBuffer = [], d = 0;

    domBuffer[d++] =
            "<div id='loadedGameslist'>" +
                "<div id='gamelistTbl' class='loadedGameWrapper'>" +
                    "<div class='row_header_loadedGame_tabs tablerow tablerowheader'>" +
                        "<div>host</div>" +
                        "<div>grid</div>" +
                        "<div>lives</div>" +
                        "<div>mine (%)</div>" +
                        "<div>Players</div>" +
                        "<div>TimetoJoin</div>" +
                    "</div>";
            
    var sortedGamesList = data.sortArray();

    for (var i = 0; i < sortedGamesList.length; i++) {
        var gameListItem = data.getGameListItem(sortedGamesList[i].id);
        var gamedata = gameListItem.getGame();
        var isGamelocked = gamedata.gameLocked;
        var gamelocked = " gamelistGameUnLocked";
        var gamephase = "";
        if (isGamelocked) {
            gamelocked = " gamelistGameLocked";
        } else {
            if(gamedata.isInGamePhase()) {
                gamephase = " inGamePhaseState";
            } else
            if(gamedata.isWinGamePhase()) {
                gamephase = " winPhaseState";
            }
        }

        var t = getTimeRemaining(gamedata.gameStart);
        var tStr = t.toString();
        if(t.total < 0) {
            tStr = "started";
        }

        domBuffer[d++] =
                    "<div class='tablerow game_row" + gamelocked +" "+ gamedata.phase +"' id='game_row_" + gamedata.getId() + "' style='order: "+i+"'>" +
                        "<div>" + gamedata.host + "</div>" +
                        "<div>" + gamedata.config.brdWidth + " X " + gamedata.config.brdHeight + "</div>" +
                        "<div>" + gamedata.config.playerLivesAmt + "</div>" +
                        "<div>" + gamedata.config.customMineAmt + "</div>" +
                        "<div>" + gamedata.getBoardCount() + " / " + gamedata.config.playerAmount + "</div>" +
                        "<div>" + tStr + "</div>" +
                    "</div>";
    }

    domBuffer[d++] =
                "</div>" +
            "</div>";

    replaceHtml(container, domBuffer.join(''));
    for (var i = 0; i < Object.keys(data._games).length; i++) {
        var game = data.getGame([Object.keys(data._games)[i]]);
        var game_row = document.getElementById("game_row_" + game.getId());
        if (!game.gameLocked) {
            applyEvents(game_row, game);
        }
    }
}

export function destroyGameList(container) {
    replaceHtml(container, "");
}

export function addGamesListToDom(data) {
    var loadedGamesDom = document.getElementById("loadedGames");
    loadGamesListHtml(loadedGamesDom, data);
}

function applyEvents(element, data) {
    var mouseEnabled = true;
    var eventType = (!mouseEnabled) ? "touchstart" : "mouseup";

    element.addEventListener(eventType, function (event) {
        destroyGameList(document.getElementById("loadedGames"));
        destroyGameForm();
        joinGame(data.getId());
    });
}