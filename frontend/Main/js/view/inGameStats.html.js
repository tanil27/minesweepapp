import {replaceHtml} from "../utils/DomUtils";

function loadInGameStatsHtml(container, data) {
    var domBuffer = [], d = 0;
    var i = 0;

domBuffer[i++] = 
        "<div id='stats'>"+
            "<div class='gameInfoContainer'>"+
                "<div class=\"totalFlagRemaining\">Total Flags Used: " + (data.fieldGrid.mineCount - data.fieldGrid.flagUsed) + "</div>" +
                "<div class=\"totalActiveMines\">Total Active Mines: " + data.fieldGrid.activeMineCount + "</div>" +
            "</div>"+
            
            "<div class='playerInfoContainerWrapper'>";
            for(var b = 0; b < Object.keys(data.fieldGrid.boards).length; b++) {
                var boardI =  data.fieldGrid.boards[Object.keys(data.fieldGrid.boards)[b]];
domBuffer[i++] =
                "<div class=\"playerInfoContainer\" id=\"playerInfoContainer_" + boardI.id + "\">" +
                "     <div class=\"playerName\">Player " + boardI.player.player + "</div>" +
                "     <div class=\"playerName\">Lives: " + boardI.hits + "</div>" +
                "     <div class=\"playerScore\">Score: " + boardI.score + "</div>" +
                "     <div class=\"flagUsed\">Flag Used: " + boardI.flagAmount + "</div>" +
                "</div>";
            }
    
domBuffer[i++] =  
            "</div>";

domBuffer[i++] =
            "</div>"+
        "</div>";
    
    replaceHtml(container, domBuffer.join(''));
}

export function destroyInGameStats(container) {
    replaceHtml(container, "");
}

export function addInGameStatsToDom(data) {
    console.log("add stats");
    var inGameStatsWrapperDom = document.getElementById("inGameStatsWrapper");
    loadInGameStatsHtml(inGameStatsWrapperDom, data);
}

function applyInGameStatsEvents(element, data) {
    var mouseEnabled = true;
    var eventType = (!mouseEnabled) ? "touchstart" : "mouseup";

    element.addEventListener("contextmenu", contextMenuHandler);

    element.addEventListener(eventType, function (event) {
    });
}