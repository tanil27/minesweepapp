import {addGameFormToDom, destroyGameForm} from "./addGameForm.jsx";
import GlobalParams from "../store/GlobalParams";
import {replaceHtml} from "../utils/DomUtils";
import {destroyGameList} from "./gameslist.html";
import {destroyInGameStats} from "./inGameStats.html";
import {leaveGame, mouseClicked} from "../app/api";
import FIELD_STATES from "../constants/FIELD_STATES";

var cellSize = 32;
    
function loadGameBoardHtml(container, data) {
    var domBuffer = [], d = 0;
    var i = 0;
domBuffer[i++] =
        "<div class='gameMenu'>"+
            "<div class='menuButton' id='inGameMenuButton'>Menu</div>"+
        "</div>";

    var board = data.fieldGrid.boards[GlobalParams.playerDataObj.getPlayer()];

    var fieldgrid = data.fieldGrid;
    
    var rs = 0, re = fieldgrid.height, cs = 0, ce = fieldgrid.width;
    var playerBoardId = "playerBoard_" + board.id;
    var wrapperId = "wrapperContainer_" + board.id;
    var fieldId = "fieldContainer_" + board.id;
    
    var win = "";
    var death = "";
    var pregame = "";
    if (data.isWinGamePhase()) {
        win = "win position_" + board.position;
    }
    
    if (!board.hits > 0) {
        death = "dead";
    }

    if(data.isPreGamePhase() || data.getJoinedLate()) {
        pregame = "pregame";
    }
    
domBuffer[i++] =
            "<div id=" + playerBoardId + " class=\"playerBoardWrapper\"> " +
                "<div id=" + wrapperId + " class=\"wrapper " + win + " " + death + " " + pregame + "\">" +
                    "<div id=" + fieldId + " class=\"field\" style='width: " + fieldgrid.width * cellSize + "px; height: " + fieldgrid.height * cellSize + "px;'>";
domBuffer[i++] =
                        "<div class=\"cellBufferContainer\" style=\"top:" + rs * cellSize + "px; left:" + cs * cellSize + "px;\">";
                    for (var y = rs; y < re; y++) {
domBuffer[i++] = 
                            "<div id=\"row" + y + "\" class=\"row\">";
                        for (var x = cs; x < ce; x++) {
                            var id = y * fieldgrid.height + x;
                            
                            var cellElid = "cell" + board.id + "_" + id;
                            var itemElid = "item" + board.id + "_" + id;

                            var cellData = fieldgrid.grid[y][x];
                            var cData = "", textContent = "";
                            if (cellData.checked) {
                                if (cellData.state >= FIELD_STATES.NUMBER) {
                                    cData = "hint number" + cellData.state;
                                    textContent = cellData.state;
                                }
                                else if (cellData.state === FIELD_STATES.EMPTY) {
                                    cData = "emptySpace";
                                }
                                else if (cellData.state === FIELD_STATES.MINE) {
                                    cData = "mine";
                                }
                            } else if (cellData.owner !== null) {
                                cData = "flag ";
                                if(fieldgrid.getPlayer(cellData.owner.getPlayer().getPlayer())) {
                                    cData+= "player" +cellData.owner.id
                                }
                            } else {
                                cData = "";
                            }
domBuffer[i++] =
                                "<div id=\"col" + x + "\" class=\"col\">" +
                                    "<div id=" + cellElid + " class=\"cell\">" +
                                        "<div id=" + itemElid + " class=\"item " + cData + "\">" + textContent + "</div>" +
                                    "</div>" +
                                "</div>";
                        }
domBuffer[i++] =
                            "</div>";
                    }                        
domBuffer[i++] =
                        "</div>";
domBuffer[i++] =
                    "</div>" +
                "</div>" +
            "</div>";
    replaceHtml(container, domBuffer.join(''));
    if(data.isInGamePhase() || data.getJoinedLate()) {
        var fgEl = document.getElementById(fieldId);
        applyFieldGridEvents(fgEl, data);
    }
    var inMenuBtn = document.getElementById("inGameMenuButton");
    applyMenuButtonEvent(inMenuBtn, data.id);
}

function destroyGameBoard(container) {
    replaceHtml(container, "");
}

export function addGameBoardToDom(data) {
    var gameListDom = document.getElementById("loadedGames");
    destroyGameList(gameListDom);
    destroyGameForm(document.getElementById("addGameFormContainer"));

    var gameWrapperDom = document.getElementById("gameBoardWrapper");
    loadGameBoardHtml(gameWrapperDom, data);
}

function contextMenuHandler(event) {
    event.preventDefault();
    return false;
}

function applyMenuButtonEvent(element, gameID) {
    element.addEventListener("click", function(e){
        var gameWrapperDom = document.getElementById("gameBoardWrapper");
        destroyGameBoard(gameWrapperDom);
        var inGameStatsWrapperDom = document.getElementById("inGameStatsWrapper");
        destroyInGameStats(inGameStatsWrapperDom);
        addGameFormToDom();
        console.log("leave game ...");
        leaveGame(gameID);
    });
}

let clickTimer = null;
function applyFieldGridEvents(element, data) {
    var mouseEnabled = true;
    var eventType = (!mouseEnabled) ? "touchstart" : "mouseup";    

    element.addEventListener("contextmenu", contextMenuHandler);

    element.addEventListener(eventType, function (event) {
        var interactiveType = (!mouseEnabled) ? event.targetTouches[0] : event;
        var mouseX = Math.floor(interactiveType.offsetX / cellSize);
        var mouseY = Math.floor(interactiveType.offsetY / cellSize);
        if(!clickTimer) {
            clickTimer = setTimeout(function(){
                mouseClicked(mouseX, mouseY, 0, data.id);
                clickTimer = null;
            },200);
        } else {
            clearTimeout(clickTimer);
            clickTimer = null;
            mouseClicked(mouseX, mouseY, 2, data.id);
        }
    });
}