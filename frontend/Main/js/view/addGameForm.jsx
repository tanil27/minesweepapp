import React from 'react';
import ReactDOM from 'react-dom';

import {replaceHtml} from "../utils/DomUtils";
import {newGame} from "../app/api";
import GameFormContainer from "msGameForm/GameForm";

let container =  document.getElementById("addGameFormContainer");

function loadAddGameFormHtml(data) {
    var domBuffer = [], d = 0;

    domBuffer[d++] =
        "<div id='formAddGame' class='formAddGameWrapper'>" +
            "<form name='addGameForm' id='addGameFormID'>"+
                "PlayerCount: <input type='number' name='fPlayerCount' required>"+
                "PlayerLives: <input type='number' name='fPlayerLives' required>"+
                "MineAmount: <input type='number' name='fMineAmount' required>"+
                "BrdWidth: <input type='number' name='fBrdWidth' required>"+
                "BrdHeight: <input type='number' name='fBrdHeight' required>"+
                "<input type='submit' value='Add Game' class='submitBtn submitAddGameForm' id='submitAddGameFormBtn'>"+
            "</form>"+
        "</div>";

    container = replaceHtml(container, domBuffer.join(''));

    var submitBtn = document.getElementById("addGameFormID");
    //debounce + event delegation function
    applyFormClickEvents(submitBtn);
}

export function destroyGameForm() {
    container = replaceHtml(container, "");
}

export function addGameFormToDom(data) {
    ReactDOM.render(<GameFormContainer
      onSubmit={({values: {playerCount, playerLives, mineAmount, brdWidth, brdHeight}}) => newGame(playerCount, playerLives, mineAmount, brdWidth, brdHeight)}
    />, container);
    //loadAddGameFormHtml(data);
}
// apply event delegation here
function applyFormClickEvents(element, data) {
    element.addEventListener("submit", function (event) {
        event.preventDefault();
        var form = document.forms["addGameForm"];
        var playerCount = parseInt(form["fPlayerCount"].value);
        var playerLives = parseInt(form["fPlayerLives"].value);
        var mineAmount = parseInt(form["fMineAmount"].value);
        var brdWidth = parseInt(form["fBrdWidth"].value);
        var brdHeight = parseInt(form["fBrdHeight"].value);
        newGame(playerCount, playerLives, mineAmount, brdWidth, brdHeight);
        return false;
    });
}