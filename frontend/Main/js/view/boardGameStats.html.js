import {replaceHtml} from "../utils/DomUtils";

function loadBoardGameStatsHtml(data) {
    var domBuffer = [], d = 0;
    var i = 0;
    for(var b = 0; b < data.boards.length; b++) {
        var boardI = data.boards[b];
        if(boardI.player.id == GlobalParams.playerDataObj) {
            var me = boardI;
        }
domBuffer[i++] =
        "<div class=\"playerInfoContainer\" id=\"playerInfoContainer_" + boardI.id + "\">" +
            "<div class=\"playerName\">Player " + boardI.player.id + "</div>" +
            "<div class=\"playerName\">Lives: " + boardI.hits + "</div>" +
            "<div class=\"playerScore\">Score: " + boardI.score + "</div>" +
            "<div class=\"flagUsed\">Flag Used: " + boardI.flagAmount + "</div>" +
        "</div>";
    }
    return domBuffer.join('');
}

function destroyGameBoardStats(container) {
    replaceHtml(container, "");
}

export function addBoardGameStatsToDom(container, data) {
    replaceHtml(container, loadBoardGameStatsHtml(data));
}

function applyBoardGameStatsEvents(element, data) {
}