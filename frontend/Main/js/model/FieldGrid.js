var FieldGrid = function (mineCount, width, height) {
    this.grid = null;
    this.groups = [];
    this.mineList = [];
    this.boards = {};
    this.width = width;
    this.height = height;
    this.mineCount = mineCount;
    this.activeMineCount = mineCount;
    this.flagUsed = mineCount;
    this.spacesLeftToReveal = width * height;
};
FieldGrid.prototype = {
    getGrid: function () {
        return this.grid;
    },
    setGrid: function (_grid) {
        this.grid = _grid;
    },
    getGroups: function () {
        return this.groups;
    },
    resetGroups: function () {
        this.groups = [];
    },
    copyGroupOver: function (copyFrom, copyTo) {
        var copy = this.groups.slice(copyFrom, copyFrom + 1)[0];
        if (copy == null) debugger;
        for (var i = 0; i < copy.length; i++) {
            copy[i].setGroup(copyTo);
        }
        if (this.groups[copyTo] == null) debugger;
        this.groups[copyTo] = this.groups[copyTo].concat(copy);
        delete this.groups[copyFrom];
    },
    getGroupByLetter: function (letter) {
        return this.groups[letter];
    },
    getCellByGroup: function (letter) {

    },
    setCellsGroup: function (x, y, group) {
        if (!this.groups[group]) {
            this.groups[group] = new Array();
        }
        this.groups[group].push(this.grid[y][x]);
        this.grid[y][x].setGroup(group);
    },
    getMineList: function () {
        return this.mineList;
    },
    setMineList: function (mineList) {
        this.mineList = mineList;
    },
    resetMineList: function () {
        this.mineList = [];
    },
    getRow: function (y) {
        return this.grid[y];
    },
    getColumn: function (x) {
        var tempColumn = {};
        for (var i = 0; i < Object.keys(this.grid).length; i++) {
            tempColumn[i] = this.getCell(i, x);
        }
        return tempColumn;
    },
    getCell: function (x, y) {
        return this.grid[y][x];
    },
    getBoards: function () {
        return this.boards;
    },
    getBoard: function (playerID) {
        return this.boards[playerID];
    },
    addBoard: function (board) {
        this.boards[board.getPlayer().getPlayer()] = board;
    },
    getPlayer: function (playerData) {
        return this.boards[playerData];
    },
    removePlayer: function (playerData) {
        delete this.boards[playerData];
    },
    getWidth: function () {
        return this.width;
    },
    getHeight: function () {
        return this.height;
    },
    getMineCount: function () {
        return this.mineCount;
    },
    getActiveMineCount: function () {
        return this.activeMineCount;
    },
    updateActiveMineCount: function (used) {
        this.activeMineCount = this.activeMineCount + used;
    },
    resetActiveMineCount: function () {
        this.activeMineCount = this.mineCount;
    },
    getFlagUsed: function () {
        return this.flagUsed;
    },
    setFlagUsed: function (used) {
        this.flagUsed = used;
    },
    updateFlagUsed: function (used) {
        this.flagUsed = this.flagUsed + used;
    },
    resetFlagUsed: function () {
        this.flagUsed = this.mineCount;
    },
    getSpacesLeftToReveal: function () {
        return this.spacesLeftToReveal;
    },
    setSpacesLeftToReveal: function (spacesLeft) {
        this.spacesLeftToReveal = spacesLeft;
    },
    updateSpacesLeftToReveal: function (spacesLeft) {
        this.spacesLeftToReveal = this.spacesLeftToReveal + spacesLeft;
    },
    resetSpacesLeftToReveal: function () {
        this.spacesLeftToReveal = this.height * this.width;
    },
    removeBoard: function (id) {
        //memory reallocation algorithm needed for optimisation
        this.boards[id] = null;
    },
    toJson: function () {
        return {
            id: this.id,
            grid: this.grid,
            boards: this.boards,
            width: this.width,
            height: this.height,
            mineCount: this.mineCount,
            activeMineCount: this.activeMineCount,
            flagUsed: this.flagUsed,
            spacesLeftToReveal: this.spacesLeftToReveal
        };
    }
};

export default FieldGrid;