import PHASE_STATES from "../constants/PHASE_STATES";
import LOBBY_SORT_CRITERIA from "../constants/LOBBY_SORT_CRITERIA";

var GameListItem = function (game) {
    this._game = game;
};

GameListItem.prototype = {
    getGame: function () {
        return this._game;
    },
    getOrder: function () {
        return this._order;
    },
    setOrder: function (order) {
        this._order = order;
    },
    getSortData: function () {
        var phaseIndex = 0;
        switch (this._game.getPhase()) {
            case PHASE_STATES.PREGAME:
                phaseIndex = 0;
                break;
            case PHASE_STATES.INGAME:
                phaseIndex = 2;
                break;
            case PHASE_STATES.WIN:
                phaseIndex = 1;
                break;
        }

        var lockedIndex = 0;
        if(this._game.gameLocked) {lockedIndex = 1;}

        var sortData = {};
        sortData[LOBBY_SORT_CRITERIA.GRID_DIMENSIONS] = this._game.getConfig().getBrdWidth() * this._game.getConfig().getBrdHeight();
        sortData[LOBBY_SORT_CRITERIA.TIME] = this._game.getGameStart();
        sortData[LOBBY_SORT_CRITERIA.PHASE] = phaseIndex;
        sortData[LOBBY_SORT_CRITERIA.LOCKED] = lockedIndex;
        sortData[LOBBY_SORT_CRITERIA.PLAYERS] = this._game.getConfig().getPlayerAmount();
        sortData[LOBBY_SORT_CRITERIA.MINES] = this._game.getConfig().getCustomMineAmt();
        sortData[LOBBY_SORT_CRITERIA.LIVES] = this._game.getConfig().getPlayerLivesAmt();
        sortData[LOBBY_SORT_CRITERIA.ID] = this._game.getId();
        return sortData;
    }
};

export default GameListItem;