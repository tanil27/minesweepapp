var PlayerData = function (id) {
    this.player = id;
};
PlayerData.prototype = {
    getPlayer: function () {
        return this.player;
    },
    toJson: function () {
        return {
            player: this.player
        };
    }
};

export default PlayerData;