import FIELD_STATES from "../constants/FIELD_STATES";

var CellData = function (id, x, y) {
    this.id = id;
    this.x = x;
    this.y = y;
    this.state = 0;
    this.checked = false;
    this.owner = null;
    this.group = null;
};
CellData.prototype = {
    setGroup: function (group) {
        this.group = (group);
    },
    getGroup: function () {
        return this.group;
    },
    hasGroup: function () {
        if (this.group !== null)return true;
        return false;
    },
    setState: function (state) {
        this.state = state;
    },

    setMine: function () {
        this.state = FIELD_STATES.MINE;
    },

    isMine: function () {
        return this.state == FIELD_STATES.MINE;
    },

    setEmpty: function () {
        this.state = FIELD_STATES.EMPTY;
    },

    isEmpty: function () {
        return this.state == FIELD_STATES.EMPTY;
    },

    isNumber: function () {
        return this.state >= FIELD_STATES.NUMBER;
    },

    setNumber: function () {
        this.state = FIELD_STATES.NUMBER;
    },

    updateNumber: function () {
        this.state = this.state + 1;
    },

    isChecked: function () {
        return this.checked;
    },

    setChecked: function (checked) {
        this.checked = checked;
    },

    isFlagged: function () {
        return this.owner != null;
    },

    setFlag: function (owner) {
        this.owner = owner;
    },

    updateFlag: function (owner) {
        this.owner = owner;
        return this.isFlagged();
    },

    getOwner: function () {
        return this.owner;
    },

    getX: function () {
        return this.x;
    },

    getY: function () {
        return this.y;
    },

    getState: function () {
        return this.state;
    },

    resetCell: function () {
        this.state = 0;
        this.checked = false;
        this.owner = null;
        this.group = null;
    },
    
    parseData: function(state, checked) {
        this.setState(state);
        this.setChecked(checked);
    },

    toJson: function () {
        var tOwn;
        if (this.owner != null) {
            tOwn = this.owner.toJson()
        } else {
            tOwn = null;
        }
        var cellDataJS = {
            id: this.id,
            x: this.x,
            y: this.y,
            state: this.state,
            checked: this.checked,
            owner: tOwn
        };
        return cellDataJS;
    }
};

export default CellData;