import LOBBY_SORT_CRITERIA from "../constants/LOBBY_SORT_CRITERIA";
import GameListItem from "./GameListItem";

export default class GameList {
  constructor() {
    this._games = {};
    this.setSortCriteria();
  }

  addGame(game) {
    if (!this._games[game.getId()]) {
      this._games[game.getId()] = new GameListItem(game);
      return game;
    }
    return null;
  }

  setPlayerCount(game) {}
  getPlayerCount(game) {}

  removeGame(game) {
    delete this._games[game.getId()];
  }

  getGameListItem(gameID) {
    return this._games[gameID] ? this._games[gameID] : null;
  }

  getGame(gameID) {
    if (this._games[gameID]) {
      return this._games[gameID].getGame();
    } else {
      return null;
    }
  }

  getGames() {
    return this._games;
  }

  toArray() {
    const arr = [];
    for (let game in this._games) {
      arr.push(this._games[game].getSortData());
    }
    return arr;
  }

  setSortCriteria(sortBy) {
    // @param: sortBy in form {property: const.prop, order: "order"}

    const remove = function (array, element) {
      const index = array.indexOf(element);

      if (index !== -1) {
        array.splice(index, 1);
      }
    };

    let tempProps = Object.values(LOBBY_SORT_CRITERIA);

    //keep default sorts to top
    let sortCriteria = [
      {property: LOBBY_SORT_CRITERIA.LOCKED, order: "asc"},
      {property: LOBBY_SORT_CRITERIA.PHASE, order: "asc"},
      {property: LOBBY_SORT_CRITERIA.TIME, order: "desc"}
    ];

    for (let i = 0; i < sortCriteria.length; i++) {
      remove(tempProps, sortCriteria[i].property);
    }

    //default sort criteria
    let mainSortBy = [
      {property: LOBBY_SORT_CRITERIA.GRID_DIMENSIONS, order: "desc"}
    ];


    if (sortBy) {
      //override default sort criteria

      //can potentially make this an array to refine sorting
      // ... keeping as single param for now
      mainSortBy = sortBy;

    }

    for (let z = 0; z < mainSortBy.length; z++) {
      let tsort = mainSortBy[z];

      let prop = tsort.property;
      let order = tsort.order;
      sortCriteria.push({
        property: prop,
        order: order
      });
      remove(tempProps, prop);
    }

    //no need to remove the elements anymore ... Garbage collection can handle this
    //otherwise switch to a while loop to ensure that removal doesn't affect the
    // for loop
    for (let i = 0; i < tempProps.length; i++) {
      let sortByCrit = {
        property: tempProps[i],
        order: "asc"
      };
      sortCriteria.push(sortByCrit);
    }

    console.log(sortCriteria);


    this.sortCriteria = sortCriteria;
  }

  sortArray() {
    let sortFunc = this.createSortFunctionForCriteria(this.sortCriteria);
    return this.toArray().sort(sortFunc);
  }

  //sorting functions
  createSortFunction(property, order) {
    let resultModifier = (order && order === 'desc') ? -1 : 1;

    return function (a, b) {
      let result = 0;

      if (a[property] < b[property]) {
        result = -1;
      }
      if (a[property] > b[property]) {
        result = 1;
      }
      return result * resultModifier;
    }
  }

  createSortFunctionForCriteria(criteria) {
    let functionMap = {};
    for (let i = 0; i < criteria.length; i++) {
      let thisCriteria = criteria[i];
      functionMap[thisCriteria.property] = this.createSortFunction(thisCriteria.property, thisCriteria.order);
    }

    return function (a, b) {
      let result;

      for (let i = 0; i < criteria.length; i++) {
        let thisCriteria = criteria[i];
        let sortFunction = functionMap[thisCriteria.property];
        result = sortFunction(a, b);
        if (result !== 0) {
          break;
        }
      }

      return result;
    };
  }


  toJson() {
    let gameListDataJS = {};
    return gameListDataJS;
  }

}