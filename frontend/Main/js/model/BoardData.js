var BoardData = function (_id, playerData) {
    this.id = _id;
    this.player = playerData;
    this.flagAmount = 0;
    this.score = 0;
    this.position = null;
    this.lives = null;
    this.hits = null;
};
BoardData.prototype = {

    getId: function () {
        return this.id;
    },

    getScore: function () {
        return this.score;
    },

    setScore: function (score) {
        this.score += score;
    },

    getPosition: function () {
        return this.position;
    },

    setPosition: function (position) {
        this.position = position;
    },

    getLives: function () {
        return this.lives;
    },

    setLives: function (lives) {
        this.lives = lives;
        this.hits = lives;
    },

    isAlive: function () {
        return this.hits > 0;
    },

    getHits: function () {
        return this.hits;
    },

    setHit: function () {
        this.hits--;
    },

    getPlayer: function () {
        return this.player;
    },

    getFlagAmount: function () {
        return this.flagAmount;
    },

    setFlagAmount: function (flagAmount) {
        this.flagAmount = flagAmount;
    },

    resetBoard: function () {
        this.hits = this.lives;
        this.score = 0;
        this.position = -1;
    },

    toJson: function () {
        return {
            id:         this.id,
            player:     this.player.toJson(),
            flagAmount: this.flagAmount,
            score:      this.score,
            position:   this.position,
            lives:      this.lives,
            hits:       this.hits
        }
    }
};

export default BoardData;