var GameConfig = function (playerAmount, playerLivesAmt, customMineAmt, brdWidth, brdHeight) {
    this.playerAmount = playerAmount;
    this.playerLivesAmt = playerLivesAmt;
    this.customMineAmt = customMineAmt;
    this.brdWidth = brdWidth;
    this.brdHeight = brdHeight;
};

GameConfig.prototype = {
    getPlayerAmount: function () {
        return this.playerAmount;
    },
    setPlayerAmount: function (playerAmount) {
        this.playerAmount = playerAmount;
    },
    getPlayerLivesAmt: function () {
        return this.playerLivesAmt;
    },
    setPlayerLivesAmt: function (playerLivesAmt) {
        this.playerLivesAmt = playerLivesAmt;
    },
    getCustomMineAmt: function () {
        return this.customMineAmt;
    },
    setCustomMineAmt: function (customMineAmt) {
        this.customMineAmt = customMineAmt;
    },
    getBrdWidth: function () {
        return this.brdWidth;
    },
    setBrdWidth: function (brdWidth) {
        this.brdWidth = brdWidth;
    },
    getBrdHeight: function () {
        return this.brdHeight;
    },
    setBrdHeight: function (brdHeight) {
        this.brdHeight = brdHeight;
    },
    toJson: function () {
        var configJS = {
            playerAmount: this.playerAmount,
            playerLivesAmt: this.playerLivesAmt,
            customMineAmt: this.customMineAmt,
            brdWidth: this.brdWidth,
            brdHeight: this.brdHeight
        };
        return configJS;
    }
};

export default GameConfig;