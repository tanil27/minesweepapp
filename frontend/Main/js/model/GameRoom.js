import PlayerData from "./PlayerData";
import FieldGrid from "./FieldGrid";
import PHASE_STATES from "../constants/PHASE_STATES";
import CellData from "./CellData";
import BoardData from "./BoardData";

var GameRoom = function (id, player, config, dateCreated) {
    this.id = id;
    this.fieldGrid = new FieldGrid(config.getCustomMineAmt(), config.getBrdWidth(), config.getBrdHeight());
    this.timeCreated = dateCreated;
    this.gameStart;
    this.phase = PHASE_STATES.PREGAME;
    this.gameLocked = false;
    this.config = config;
    this.host = player;
    this.joinedLate = false;
    this.boardCount = 0;
};

GameRoom.prototype = {
    getId: function() {
        return this.id;
    },
    setId: function (id) {
        this.id = id;
    },
    getFieldGrid: function() {
        return this.fieldGrid;
    },
    setFieldGrid: function (fieldGrid) {
        this.fieldGrid = fieldGrid;
    },
    getBoardCount: function(){
        return this.boardCount;
    },
    setBoardCount: function (boardCount) {
        this.boardCount = boardCount;
        if(this.boardCount == this.config.getPlayerAmount()) {
            this.setGameLocked(true);
        }
    },
    getJoinedLate: function () {
        return this.joinedLate;
    },
    setJoinedLate: function(bool) {
        this.joinedLate = bool;
    },
    getTimeCreated: function() {
        return this.timeCreated;
    },
    setTimeCreated: function (timeCreated) {
        this.timeCreated = timeCreated;
    },
    getGameStart: function() {
        return this.gameStart;
    },
    setGameStart: function (gameStart) {
        this.gameStart = gameStart;
    },
    getGameLocked: function() {
        return this.gameLocked;
    },
    setGameLocked: function (gameLocked) {
        this.gameLocked = gameLocked;
    },
    getConfig: function() {
        return this.config;
    },
    setConfig: function (config) {
        this.config = config;
    },
    getHost: function() {
        return this.host;
    },
    setHost: function (host) {
        this.host = host;
    },
    getPhase: function () {
        return this.phase;
    },
    setPhase: function (phase) {
        this.phase = phase;
    },

    isPreGamePhase: function (){
        return this.phase == PHASE_STATES.PREGAME;
    },
    isInGamePhase: function (){
        return this.phase == PHASE_STATES.INGAME;
    },
    isWinGamePhase: function (){
        return this.phase == PHASE_STATES.WIN;
    },

    setupGame: function () {
        this.createMasterBoard();
        this.reset();
    },
    createMasterBoard: function () {
        var i = 0;
        var fieldGrid = new Array(this.getFieldGrid().getHeight());
        for (var y = 0; y < this.getFieldGrid().getHeight(); y++) {
            fieldGrid[y] = new Array(this.getFieldGrid().getWidth());
            for (var x = 0; x < this.getFieldGrid().getWidth(); x++) {
                fieldGrid[y][x] = new CellData(i, x, y);
                i++;
            }
        }
        this.fieldGrid.setGrid(fieldGrid);
    },
    initBoardState: function (minelist) {
        this.fieldGrid.setMineList(minelist);
        this.setHintNumbers();
        this.setEmptySpaces();
    },
    setHintNumbers: function() {
        var checkConnections = function(x, y, fieldGrid) {
            if (fieldGrid[y] && fieldGrid[y][x]) {
                var cell = fieldGrid[y][x];
                if (!cell.isMine()) {
                    cell.updateNumber();
                }
            }
        };
        var positions = [[-1, 0], [-1, -1], [0, -1], [1, -1], [1, 0], [1, 1], [0, 1], [-1, 1]];
        var xPos;
        var yPos;
        var mineList = this.fieldGrid.getMineList();
        var fieldGrid = this.fieldGrid.getGrid();
        for (var i = 0; i < mineList.length; i++) {
            for (var r = 0; r < positions.length; r++) {
                xPos = mineList[i].getX();
                yPos = mineList[i].getY();
                checkConnections(xPos + positions[r][0], yPos + positions[r][1], fieldGrid);
            }
        }
    },
    setEmptySpaces: function() {
        var isCellInBounds = function(x, y) {
            if (x < 0) {
                return false;
            }
            if (y < 0) {
                return false;
            }
            if (x > this.fieldGrid.getWidth() - 1) {
                return false;
            }
            if (y > this.fieldGrid.getHeight() - 1) {
                return false;
            }
            return true;
        }.bind(this);

        var cell, topCell, leftCell, rightCell, bottomCell, _x, _y, x_, y_, emptyCount;
        var changedGroups = [];
        this.fieldGrid.resetGroups();

        var setNumberObj = {};

        for (var y = 0; y < this.fieldGrid.getHeight(); y++) {
            for (var x = 0; x < this.fieldGrid.getWidth(); x++) {

                cell = this.fieldGrid.getCell(x, y);

                setNumberObj = {
                    top: {group: false, cell: null},
                    left: {group: false, cell: null},
                    right: {group: false, cell: null},
                    bottom: {group: false, cell: null},
                    center: {group: true, cell: null}
                };
                setNumberObj.center.cell = cell;

                topCell = null;
                leftCell = null;
                rightCell = null;
                bottomCell = null;

                if (!cell.isMine() && !cell.isNumber()) {

                    _x = x - 1;
                    _y = y - 1;
                    x_ = x + 1;
                    y_ = y + 1;
                    changedGroups = [];
                    emptyCount = 0;
                    var tempGroup = this.fieldGrid.getGroups().length;

                    //get Temp Group
                    if (isCellInBounds(x, _y)) {
                        //neighbour cell: top
                        topCell = this.fieldGrid.getCell(x, _y);

                        if (topCell.isEmpty() && topCell.hasGroup()) {
                            if (topCell.getGroup() > tempGroup) {
                            } else {
                                tempGroup = topCell.getGroup();
                            }
                            changedGroups.push(topCell.getGroup());
                        }
                        if (topCell.isNumber()) {
                            setNumberObj.top.group = true;
                            setNumberObj.top.cell = topCell;
                        }
                    }
                    if (isCellInBounds(_x, y)) {
                        //neighbour cell: left
                        leftCell = this.fieldGrid.getCell(_x, y);
                        if (leftCell.isEmpty() && leftCell.hasGroup()) {
                            if (leftCell.getGroup() > tempGroup) {
                            } else {
                                tempGroup = leftCell.getGroup();
                            }
                            changedGroups.push(leftCell.getGroup());
                        }
                        if (leftCell.isNumber()) {
                            setNumberObj.left.group = true;
                            setNumberObj.left.cell = leftCell;
                        }
                    }

                    if (isCellInBounds(x_, y)) {
                        //neighbour cell: right
                        rightCell = this.fieldGrid.getCell(x_, y);
                        if (rightCell.isEmpty() && rightCell.hasGroup()) {
                            if (rightCell.getGroup() > tempGroup) {
                            } else {
                                tempGroup = rightCell.getGroup();
                            }
                            changedGroups.push(rightCell.getGroup());
                        }
                        if (rightCell.isNumber()) {
                            setNumberObj.right.group = true;
                            setNumberObj.right.cell = rightCell;
                        }
                    }
                    if (isCellInBounds(x, y_)) {
                        //neighbour cell: bottom
                        bottomCell = this.fieldGrid.getCell(x, y_);
                        if (bottomCell.isEmpty() && bottomCell.hasGroup()) {
                            if (bottomCell.getGroup() > tempGroup) {
                            } else {
                                tempGroup = bottomCell.getGroup();
                            }
                            changedGroups.push(bottomCell.getGroup());
                        }
                        if (bottomCell.isNumber()) {
                            setNumberObj.bottom.group = true;
                            setNumberObj.bottom.cell = bottomCell;
                        }
                    }


                    //check if it should be grouped
                    for (var i = 0; i < Object.keys(setNumberObj).length; i++) {
                        if (setNumberObj[Object.keys(setNumberObj)[i]].group) {
                            this.fieldGrid.setCellsGroup(setNumberObj[Object.keys(setNumberObj)[i]].cell.getX(), setNumberObj[Object.keys(setNumberObj)[i]].cell.getY(), tempGroup);
                        }
                    }

                    for (var i = 0; i < changedGroups.length; i++) {
                        if (changedGroups[i] !== tempGroup) {
                            this.fieldGrid.copyGroupOver(changedGroups[i], tempGroup);
                        }
                    }
                }
            }
        }
    },
    reset: function () {
        this.fieldGrid.resetFlagUsed();
        this.fieldGrid.resetActiveMineCount();
        this.fieldGrid.resetSpacesLeftToReveal();

        for (var i = 0; i < Object.keys(this.fieldGrid.getBoards()).length; i++) {
            var board = this.fieldGrid.getBoard(Object.keys(this.fieldGrid.getBoards())[i]);
            board.resetBoard();
            board.setFlagAmount(0);
        }

        this.resetCells();
    },
    resetCells: function () {
        for (var y = 0; y < this.fieldGrid.getHeight(); y++) {
            for (var x = 0; x < this.fieldGrid.getWidth(); x++) {
                this.fieldGrid.getCell(x, y).resetCell();
            }
        }
    },
    addPlayer: function (boardID, player) {
        if(typeof player != "object") {
            player = new PlayerData(player);
        }
        if(!this.fieldGrid.getBoard(player.getPlayer())) {
            var board = new BoardData(boardID, player);
            board.setLives(this.config.getPlayerLivesAmt());
            this.fieldGrid.addBoard(board);
        }
    },
    mouseUpEventHandler: function (x, y, mouseButton, boardData) {
        
        var cell = this.fieldGrid.getCell(x, y);
        if (cell) {
            if (mouseButton == 0) {
                if (!cell.isFlagged() && !cell.isChecked()) {
                    if (cell.isMine()) {
                        cell.setChecked(true);
                        this.fieldGrid.updateActiveMineCount(-1);
                        this.fieldGrid.updateSpacesLeftToReveal(-1);
                        this.setBoardDeath(boardData);

                        if (this.isLastManStanding()) {
                            this.doGameOver();
                        }
                    }
                    else if (cell.isNumber()) {
                        cell.setChecked(true);
                        this.fieldGrid.updateSpacesLeftToReveal(-1);
                    }
                    else {
                        var grouping = this.fieldGrid.getGroupByLetter(cell.getGroup());
                        for (var i = 0; i < grouping.length; i++) {
                            if (!grouping[i].isFlagged() && !grouping[i].isChecked()) {
                                this.fieldGrid.updateSpacesLeftToReveal(-1);
                                grouping[i].setChecked(true);
                            }
                        }
                    }
                }
            } else {
                var cplayer = boardData;
                if (!cell.isChecked()) {
                    if (cell.getOwner() == null || cell.getOwner() == cplayer) {
                        if (!cell.isFlagged()) {
                            cell.updateFlag(cplayer);
                            this.fieldGrid.updateFlagUsed(-1);
                            this.fieldGrid.updateSpacesLeftToReveal(-1);
                            boardData.setFlagAmount(boardData.getFlagAmount() + 1);
                        } else {
                            cell.updateFlag(null);
                            this.fieldGrid.updateFlagUsed(1);
                            this.fieldGrid.updateSpacesLeftToReveal(1);
                            boardData.setFlagAmount(boardData.getFlagAmount() - 1);
                        }
                    }
                }
            }
        }
        if (this.fieldGrid.getSpacesLeftToReveal() == 0) {
            this.doGameOver();
        }
    },
    setBoardDeath: function(boardData) {
        boardData.setHit();
        boardData.setScore(-this.fieldGrid.getMineCount());
    },
    isLastManStanding: function() {
        var maxDeath = this.fieldGrid.getBoards().length;
        var deathCount = 0;
        if(maxDeath == 1) {
            //1 player game
             return this.fieldGrid.getBoard(Object.keys(this.fieldGrid.getBoards())[0]).isAlive();
        }
        for(var i = 0; i < maxDeath; i++) {
            if(!this.fieldGrid.getBoard(i).isAlive()) {
                deathCount++;
            }
            if(deathCount == maxDeath - 1) {
                return true;
            }
        }
        return false;

    },
    doGameOver: function() {
        this.revealField();
        this.getWinner();
    },
    revealField: function() {
        var correctMinesFound = 0;
        for (var y = 0; y < this.fieldGrid.getWidth(); y++) {
            for (var x = 0; x < this.fieldGrid.getHeight(); x++) {
                //j
                var cell = this.fieldGrid.getCell(x, y);

                if (!cell.isFlagged() && !cell.isChecked()) {
                    cell.setChecked(true);
                }

                if (cell.isFlagged()) {
                    if (cell.getOwner().isAlive()) {
                        if (cell.isMine()) {
                            cell.getOwner().setScore(1);
                        }
                        else {
                            cell.getOwner().setScore(-1);
                        }
                    }
                }
            }
        }
    },
    getWinner: function() {
        var groupList = {};


        for (var i = 0; i < Object.keys(this.fieldGrid.getBoards()).length; i++) {
            var board = this.fieldGrid.getBoard(Object.keys(this.fieldGrid.getBoards())[i]);
            if (!groupList[board.getScore()]) {
                groupList[parseInt(board.getScore())] = [];
            }
            groupList[parseInt(board.getScore())].push(board);
        }

        var keys = Object.keys(groupList).sort(function (a, b) {
            return parseInt(b) - parseInt(a);
        });

        for (var i = 0; i < keys.length; i++) {
            var group = groupList[keys[i]];
            for (var j = 0; j < group.length; j++) {
                this.setPlace(group[j], i);
            }
        }
    },
    setPlace: function(board, pos) {
        board.setPosition(pos);
    },

    gameOver: function() {

        }
};

export default GameRoom;