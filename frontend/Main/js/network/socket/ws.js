import {onMessage, onOpen} from "../../app/app";

let socket = {
  ws: null,
  send: (msg) => sendWsMessage(msg),
  lastConnected: null,
  type: 'websocket',
  fallback: false,
  connected: false,
  local: false,
  connect: () => initWS()
}
/*
TODO:
[ ] Hide local socket behind flag --dev or something like that
[ ] use cloud instance
 */
const initWS = (openCB, closeCB) =>
{
  if (!socket.connected)
  {
    socket.ws = socket.local
      ? new WebSocket("ws://" + document.location.hostname + ":" + 8080 + "/game")
      : new WebSocket("wss://minesweep-gcd-wlogxabuga-nw.a.run.app/game");

    socket.ws.addEventListener("message", function (e) {
      onMessage(e);
    });
    socket.ws.addEventListener("open", function (e) {
      socket.connected = true;
      onOpen(e);
    });

    socket.ws.addEventListener("close", function (e) {
      console.log("Your connection has ended or been closed:" , e);
      socket.connected = false;
    });

    socket.ws.addEventListener("error", function (e) {
      socket.connected = false;
    });
  }
}

const sendWsMessage = (message) =>
{
  console.log("Message To Server: ", message);
  socket.ws.send(JSON.stringify(message));
};

export default socket;