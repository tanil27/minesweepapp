
var publisher =
        {
            subscribe: function (obj)
            {
                if (obj)
                {
                    // Don't add an subscriber more than once
                    for (var i = 0; i < this.subscribers.length; i++)
                    {

                        if (this.subscribers[i] == obj) {
                            return false;
                        }
                    }

                    this.subscribers.push(obj);
                }
            },

            unsubscribe: function (obj)
            {
                for (var i = 0; i < this.subscribers.length; i++)
                {
                    if (this.subscribers[i] === obj)
                    {
                        this.subscribers.splice(i, 1);
                    }
                }
            },

            publish: function (data)
            {
                if (!data || !data.type || typeof (data.type) !== "string")
                    throw new Error("All published updates must contain a 'type' as string");

                for (var i = 0; i < this.subscribers.length; i++)
                {
                    try
                    {
                        if (this.subscribers[i])
                        {
                            this.subscribers[i].handlePublisherUpdate(data);
                        }
                    } catch (e)
                    {
                        console.log("Error in subscriber (index " + i + "): " + this.subscribers[i], e);
                    }
                }
            },
            makePublisher: function (obj)
            {
                for (var i in publisher)
                {
                    if (!obj.hasOwnProperty(i) && publisher.hasOwnProperty(i) && typeof publisher[i] === "function")
                    {
                        obj[i] = publisher[i];
                    }
                }

                if (!obj.subscribers)
                {
                    obj.subscribers = [];
                }
            },

            destroyPublisher: function (obj)
            {
                for (var i in publisher)
                {
                    if (publisher.hasOwnProperty(i) && typeof publisher[i] === "function")
                    {
                        delete obj[i];
                    }
                }
                obj.subscribers = [];
            }
        };




