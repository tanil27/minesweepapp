const FIELD_STATES = {
  CHECKED: -3,
  FLAGGED: -2,
  MINE: -1,
  EMPTY: 0,
  NUMBER: 1
};

export default FIELD_STATES;