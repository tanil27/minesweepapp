const LOBBY_SORT_CRITERIA = {
    GRID_DIMENSIONS : "gridSize",
    TIME : "starttime",
    PHASE : "phase",
    LOCKED : "gameLocked",
    PLAYERS : "players",
    MINES: "minecount",
    LIVES: "lives",
    ID: "id"
};

export default LOBBY_SORT_CRITERIA;