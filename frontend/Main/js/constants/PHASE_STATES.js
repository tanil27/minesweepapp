const PHASE_STATES = {
    PREGAME : "pregame_phase",
    INGAME : "ingame_phase",
    WIN : "win_phase"
};

export default PHASE_STATES;