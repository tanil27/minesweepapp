import GameList from "../model/GameList";

const GlobalParams = {
  inLobby: true,
  longestSyncTime: 0,
  gameRoomsList: new GameList(),
  activeGame: null,
  playerDataObj: null,
  appTime: null,
  lobbyRenderer: null,
  networkLatency: []
};

export default GlobalParams;
