import '../css/gamelist.css';
import '../css/main.css';

import socket from "./network/socket/ws";
import {createFilteredSocket} from "../filters";
import {renderPage} from "./app/app";

/*
TODO:
  [ ] handle filter connection more streamlined
  [ ] make filter check a flag in webpack and package.json : i.e. npm run dev --filters
 */

window.addEventListener("load", function() {
  if(socket.fallback) {
    createFilteredSocket({error: 1008, msg: "force filters"});
    socket.ws.connect();
  } else {
    socket.connect();
  }
  renderPage();
});