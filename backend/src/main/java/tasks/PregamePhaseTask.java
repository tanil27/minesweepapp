/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tasks;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import model.GameRoom;

/**
 *
 * @author tanil
 */
public class PregamePhaseTask {

    private final GameRoom game;
    private final TimerTask task;
    private final Timer timer = new Timer();
    private final int delay;
    private CompletableFuture<GameRoom> future;
    
    public PregamePhaseTask(GameRoom game, int delay) {
        this.game = game;
        this.delay = delay;
        this.game.setGameStart(LocalDateTime.now().plus(delay, ChronoUnit.MILLIS));
        this.future = new CompletableFuture<>();
        this.task = new TimerTask() {
            @Override
            public void run() {
                game.cancelPregamePhase();
            }
        };
        this.timer.schedule(this.task, this.delay);
    }
    
    public CompletableFuture<GameRoom> onComplete() throws InterruptedException, ExecutionException{
        return this.future;
    }
    
    public int getElapsedTime() {
        return 0;
    }
    
    public void cancelPregamePhase(){
        future.complete(game);
        this.task.cancel();
        this.timer.cancel();
        this.future.cancel(true);
    }
    
    
}
