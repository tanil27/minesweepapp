/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constants;

/**
 *
 * @author tanil
 */
public class FIELD_STATES {
    public static final int CHECKED = -3;
    public static final int FLAGGED = -2;
    public static final int MINE = -1;
    public static final int EMPTY = 0;
    public static final int NUMBER = 1;
}
