/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socket;

import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author tanil
 */
@ApplicationScoped
@ServerEndpoint("/game")
public class GameWebSocketServer {

    @Inject
    private GameSessionHandler sessionHandler;
    
    @OnOpen
    public void open(Session session) {
        sessionHandler.addSession(session);
    }

    @OnError
    public void onError(Throwable error) {
        Logger.getLogger(GameWebSocketServer.class.getName()).log(Level.SEVERE, null, error);
    }

    @OnClose
    public void close(Session session) {
        sessionHandler.removeSession(session);
    }
    
    @OnMessage
    public void handleMessage(String message, Session session) {
        try (JsonReader reader = Json.createReader(new StringReader(message))) {
            JsonObject jsonMessage = reader.readObject();
            
            System.out.println("Incoming Message from Client("+session.getId()+"): " + message);
            
            switch(jsonMessage.getString("action")) {
                
                case "newGame" :
                    sessionHandler.newGame(jsonMessage, session);
                    break;
                    
                case "joinGame" :
                    sessionHandler.joinGame(jsonMessage.getInt("gameID"), session);
                    break;
                    
                case "mouseClick" :
                    sessionHandler.mouseClicked(
                            jsonMessage.getInt("x"),
                            jsonMessage.getInt("y"),
                            jsonMessage.getInt("mouseButton"),
                            session,
                            jsonMessage.getInt("gameID")
                    );
                    break;
                        
                case "getGamesList" :
                    sessionHandler.sendGamesList(session);
                    break;
                    
                case "restartGame" :
                    sessionHandler.restartGame(
                        jsonMessage.getInt("gameID"),
                        session
                    );
                    break;
                    
                case "leaveGame" :
                    sessionHandler.leaveGame(
                            session, 
                            jsonMessage.getInt("gameID")
                    );
                    break;
                    
                case "admin" : 
                    sessionHandler.adminAccess(session, jsonMessage.getString("passcode"));
                    break;
                
                default :
                    System.err.println("Action does not exist!");
                    break;
            }
        }
    }
}
