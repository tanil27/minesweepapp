/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socket;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonStructure;
import javax.json.spi.JsonProvider;
import javax.websocket.Session;
import model.BoardData;
import model.CellData;
import model.GameConfig;
import model.GameRoom;
import model.PlayerData;

/**
 *
 * @author tanil
 */
@ApplicationScoped
public class GameSessionHandler {

    private int gameId = 0;
    private final Map<Session, PlayerData> sessions = new HashMap<>();
    private final Map<PlayerData, ArrayList<GameRoom>> game_sessions = new HashMap<>();
    private final Map<Integer, GameRoom> games = new HashMap<>();
    private Session admin = null;

    public void adminAccess(Session session, String passcode) {
        JsonProvider provider = JsonProvider.provider();
        if(admin !=null && admin.equals(session)) {
            JsonArrayBuilder sessionArrBuilder = provider.createArrayBuilder();
            sessions.forEach((thisSession, playerData) -> {
                JsonObject sess = provider.createObjectBuilder()
                        .add("player", playerData.toJson())
                        .add("session", thisSession.toString())
                        .build();
                sessionArrBuilder.add(sess);
            });
            JsonArray sessArr = sessionArrBuilder.build();
        
            JsonObject loggedInUsersJS = provider.createObjectBuilder()
            .add("action", "admin")
            .add("adAction", "accessRequest")
            .add("sessions", sessArr)
            .add("time", LocalDateTime.now().toString())
            .build();
            
            sendToSession(admin,loggedInUsersJS);
        } else if("testpass".equals(passcode)) {
            if(admin == null){
                admin = session;
                JsonArrayBuilder sessionArrBuilder = provider.createArrayBuilder();
                sessions.forEach((thisSession, playerData) -> {
                    JsonObject sess = provider.createObjectBuilder()
                            .add("player", playerData.toJson())
                            .add("session", thisSession.getId())
                            .build();
                    sessionArrBuilder.add(sess);
                });
                JsonArray sessArr = sessionArrBuilder.build();

                JsonObject loggedInUsersJS = provider.createObjectBuilder()
                .add("action", "admin")
                .add("adAction", "accessRequest")
                .add("sessions", sessArr)
                .add("time", LocalDateTime.now().toString())
                .build();

                sendToSession(admin,loggedInUsersJS);
            } else {
                JsonObject accessRequestJS = provider.createObjectBuilder()
                .add("action", "admin")
                .add("adAction", "accessRequestError")
                .add("session", session.getId())
                .add("time", LocalDateTime.now().toString())
                .build();
                sendToSession(admin, accessRequestJS);
            }
        }
    }
    
    public void addSession(Session session) {
        PlayerData player = new PlayerData();
        player.setId(session.getId());
        sessions.put(session, player);
        
        sendToSession(session, createSessionMessage(player));
    }

    public void sendGamesList(Session session) {
        sendToSession(session, createGamesListMessage());
    }
    
    public void leaveGame(Session session, int gameID) {
        GameRoom gametoLeave = getGameById(gameID);
        PlayerData player = sessions.get(session);
        ArrayList<GameRoom> joinedGames = game_sessions.get(player);
        joinedGames.forEach((GameRoom game) -> {
            if (game.equals(gametoLeave)) {
                Boolean didRemove = game.removePlayer(player.getId());
                if (game.getFieldGrid().getBoards().isEmpty()) {
                    games.remove(game.getId());
                    game.getPregamePhase().cancelPregamePhase();
                } else {
                    PlayerData newHost = game.getFieldGrid().getBoard(0).getPlayer();
                    game.setHost(newHost);
                    game.setGameLocked(false);
                    //sendToAllInGameSessions(game, createLeaveGameMessage(game, player));
                }
                sendToAllConnectedSessions(createLeaveGameMessage(game, player));
            }
        });
        
        joinedGames.remove(gametoLeave);
        if (joinedGames.isEmpty()) {
            game_sessions.remove(player);
        }
    }

    public void removeSession(Session session) {
        PlayerData player = sessions.get(session);
        sessions.remove(session);
        ArrayList<GameRoom> joinedGames = game_sessions.get(player);
        joinedGames.forEach((GameRoom game) -> {
            Boolean didRemove = game.removePlayer(player.getId());
            if (game.getFieldGrid().getBoards().isEmpty()) {
                games.remove(game.getId());
                game.getPregamePhase().cancelPregamePhase();
            } else {
                PlayerData newHost = game.getFieldGrid().getBoard(0).getPlayer();
                game.setHost(newHost);
            }
            sendToAllConnectedSessions(createLeaveGameMessage(game, player));
        });

        game_sessions.remove(player);
    }

    public void newGame(JsonObject config, Session session) {
        PlayerData player = sessions.get(session);
        GameRoom game = new GameRoom(player);

        GameConfig gameconfig = new GameConfig(
                config.getInt("playerCount"),
                config.getInt("playerLives"),
                config.getInt("mineCount"),
                config.getJsonArray("gridDimension").getInt(0),
                config.getJsonArray("gridDimension").getInt(1)
        );

        game.setupGame(gameconfig);
        game.setId(gameId);

        games.put(gameId, game);
        sendToAllConnectedSessions(createNewGameMessage(game));
        joinGame(gameId, session);

        try {
            game.getPregamePhase().onComplete().thenAccept(g -> {
                sendGamePhaseAction(g);
            });
        } catch (InterruptedException | ExecutionException ex) {
            Logger.getLogger(GameSessionHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        gameId++;
    }

    private void sendGamePhaseAction(GameRoom game) {
        sendToAllConnectedSessions(createGamePhaseMessage(game));
        if(game.isPreGamePhase()) {
            sendToAllInGameSessions(game, createMinelistUpdateMessage(game));
        }
    }

    public void joinGame(int index, Session session) {
        GameRoom game = games.get(index);
        PlayerData player = sessions.get(session);

        Boolean joined = game.addPlayer(player);
        if (joined) {
            ArrayList<GameRoom> gameRooms;

            if (game_sessions.containsKey(player)) {
                gameRooms = game_sessions.get(player);
            } else {
                gameRooms = new ArrayList<>();
            }
            gameRooms.add(game);
            game_sessions.put(player, gameRooms);
            //send all boards to new player
            sendToSession(session, createJoinGameMessage(game, player));
            if(game.isPreGamePhase()) {
                sendToSession(session, createMinelistUpdateMessage(game));
            }
            //send new player to all other sessions
            sendToAllConnectedSessions(createGameBoardsMessage(game, player));
        } else {
            sendToSession(session, createJoinGameMessageError());
        }
    }

    public void mouseClicked(int x, int y, int mouseButton, Session session, int gameID) {
        GameRoom game = getGameById(gameID);
        PlayerData player = sessions.get(session);
        BoardData boardData = game.getFieldGrid().getBoardByPlayerID(player.getId());

        if (boardData != null) {
            if (game.isInGamePhase()) {
                game.mouseUpEventHandler(x, y, mouseButton, boardData);
                Boolean isGameOver = false;
                if (game.getWinGamePhase() != null) {
                    try {
                        isGameOver = true;
                        //Win game phase
                        sendGamePhaseAction(game);
                        game.getWinGamePhase().onComplete().thenAccept(gameWin -> {
                            // pre game phase
                            sendToAllInGameSessions(game, createMinelistUpdateMessage(game));
                            sendGamePhaseAction(gameWin);
                            try {
                                gameWin.getPregamePhase().onComplete().thenAccept(gameStart -> {
                                    //in game phase
                                    sendGamePhaseAction(gameStart);
                                });
                            } catch (InterruptedException ex) {
                                Logger.getLogger(GameSessionHandler.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (ExecutionException ex) {
                                Logger.getLogger(GameSessionHandler.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        });
                    } catch (InterruptedException | ExecutionException ex) {
                        Logger.getLogger(GameSessionHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                sendToAllInGameSessions(game, createMouseClickedMessage(x,y,mouseButton,game,player,isGameOver));
            } else {
                JsonProvider provider = JsonProvider.provider();
                JsonObject mouseClickedMessageError = provider.createObjectBuilder()
                        .add("action", "mouseClicked")
                        .add("error", "game locked")
                        .build();
                sendToSession(session, mouseClickedMessageError);
            }

        } else {
            //send updated game JSON back to 
            JsonProvider provider = JsonProvider.provider();
            JsonObject mouseClickedMessageError = provider.createObjectBuilder()
                    .add("action", "mouseClicked")
                    .add("error", "you aren't in this game")
                    .build();
            sendToSession(session, mouseClickedMessageError);
        }
    }

    public void restartGame(int id, Session session) {
        GameRoom game = getGameById(id);
        game.reset();
        JsonProvider provider = JsonProvider.provider();
        JsonObject restartGameMessage = provider.createObjectBuilder()
                .add("action", "restartGame")
                .add("game", game.toJson())
                .build();
        sendToAllInGameSessions(game, restartGameMessage);
    }

    public void removeGame(int id) {
        GameRoom game = getGameById(id);
        if (game != null) {
            games.remove(game);
            JsonProvider provider = JsonProvider.provider();
            JsonObject removeMessage = provider.createObjectBuilder()
                    .add("action", "finished")
                    .add("id", id)
                    .build();
            sendToAllConnectedSessions(removeMessage);
        }
    }

    private GameRoom getGameById(int id) {
        return games.get(id);
    }
    
    private JsonObject createSessionMessage(PlayerData player) {        
        JsonProvider provider = JsonProvider.provider();
        JsonObject addSessionMessage = provider.createObjectBuilder()
                .add("action", "connected")
                .add("player", player.getId())
                .add("time", LocalDateTime.now().toString())
                .build();
        return addSessionMessage;
    }
    
    private JsonObject createGamesListMessage() {
        JsonProvider provider = JsonProvider.provider();
        JsonArrayBuilder gamesMessage = provider.createArrayBuilder();
        
        for (Integer gamesId : games.keySet()) {
            GameRoom game = games.get(gamesId);
            JsonArray boardsJs =  createBoardsArrayJS(game);
            JsonObject gameMessage = provider.createObjectBuilder()
                    .add("id", gamesId)
                    .add("host", game.getHost().getId())
                    .add("timeCreated", game.getTimeCreated().toString())
                    .add("gameStart", game.getGameStart().toString())
                    .add("config", game.getConfig().toJson())
                    .add("locked", game.isGameLocked())
                    .add("phase", game.getPhase())
                    .add("boardCount", game.getFieldGrid().getBoards().size())
                    .add("boards", boardsJs)
                    .build();

            gamesMessage.add(gameMessage);
        }
        JsonArray gamesListJS = gamesMessage.build();

        JsonObject gamesListMessage = provider.createObjectBuilder()
                .add("action", "gamesListUpdate")
                .add("games", gamesListJS)
                .build();
        return gamesListMessage;
    }

    private JsonArray createBoardsArrayJS(GameRoom game) {            
        JsonProvider provider = JsonProvider.provider();
        JsonArrayBuilder boardsArrBuilder = provider.createArrayBuilder();
        game.getFieldGrid().getBoards().forEach(board -> {
            boardsArrBuilder.add(board.toJson());
        });
        JsonArray boardsArr = boardsArrBuilder.build();
        return boardsArr;
    }
    
    private JsonObject createJoinGameMessageError() {
        JsonProvider provider = JsonProvider.provider();
        JsonObject joinGameMessageError = provider.createObjectBuilder()
                .add("action", "joinGame")
                .add("error", "you can't join this game")
                .build();
        return joinGameMessageError;
    }

    private JsonObject createNewGameMessage(GameRoom game) {
        JsonProvider provider = JsonProvider.provider();
        JsonObjectBuilder gameJSbuild = provider.createObjectBuilder()
                .add("id", game.getId())
                .add("timeCreated", game.getTimeCreated().toString())
                .add("gameStart", game.getGameStart().toString())
                .add("locked", game.isGameLocked())
                .add("fieldGrid", game.getFieldGrid().toJson())
                .add("config", game.getConfig().toJson())
                .add("host", game.getHost().getId())
                .add("phase", game.getPhase());                
        
        JsonObject newGameMessage = provider.createObjectBuilder()
                .add("action", "newGame")
                .add("game", gameJSbuild.build())
                .build();
        return newGameMessage;
    }
    
    private JsonObject createGamePhaseMessage(GameRoom game) {
        JsonProvider provider = JsonProvider.provider();
        JsonObjectBuilder gamePhaseJSbuild = provider.createObjectBuilder()
                .add("action", "phaseUpdate")
                .add("gameID", game.getId())
                .add("phase", game.getPhase())
                .add("gameStart", game.getGameStart().toString())
                .add("locked", game.isGameLocked());
        
        JsonObject gamePhaseJS = gamePhaseJSbuild
                .build();
        return gamePhaseJS;
    }

    private JsonObject createJoinGameMessage(GameRoom game, PlayerData player) {
        JsonProvider provider = JsonProvider.provider();
        JsonObjectBuilder joinGameJSbuild = provider.createObjectBuilder()
                .add("action", "joinGame")
                .add("gameID", game.getId())
                .add("started", game.isGameLocked())
                .add("playerID", player.getId())
                .add("boards", createBoardsArrayJS(game))
                .add("newBoard", game.getFieldGrid().getBoardByPlayerID(player.getId()).toJson());
                
        return joinGameJSbuild.build();
    }
    
    private JsonObject createMinelistUpdateMessage(GameRoom game) {
        JsonProvider provider = JsonProvider.provider();
        
        JsonObjectBuilder minelistUpdateMessageBuild = provider.createObjectBuilder();
        JsonArrayBuilder jsMineArrBuilder = provider.createArrayBuilder();
        for(CellData mine : game.getFieldGrid().getMineList()) {
            JsonObject jsMinecood = provider.createObjectBuilder()
                    .add("x", mine.getX())
                    .add("y", mine.getY())
                    .build();
            jsMineArrBuilder.add(jsMinecood);
        }
        JsonArray minelistArr = jsMineArrBuilder.build();
        minelistUpdateMessageBuild
            .add("action", "minelistUpdate")
            .add("gameID", game.getId())
            .add("minelist", minelistArr);
        
        return minelistUpdateMessageBuild.build();   
    }
    
    private JsonObject createMouseClickedMessage(int x, int y, int mouseButton, GameRoom game, PlayerData player, Boolean isGameOver) {
        JsonProvider provider = JsonProvider.provider();
        JsonObject coordJS = provider.createObjectBuilder()
                .add("x", x)
                .add("y", y)
                .build();

        
        //send updated game JSON back to 
        JsonObject mouseClickedMessage = provider.createObjectBuilder()
                .add("action", "mouseClicked")
                .add("gameID", game.getId())
                .add("coord", coordJS)
                .add("clickedBy", player.toJson())
                .add("mouseBtn", mouseButton)
                .add("gameOver", isGameOver)
                .build();
        
        return mouseClickedMessage;
    }
    
    private JsonObject createGameBoardsMessage(GameRoom game, PlayerData player) {
        JsonProvider provider = JsonProvider.provider();
        JsonObjectBuilder joinGameMessage = provider.createObjectBuilder();
        JsonObject joinGameJS = joinGameMessage
                .add("action", "joinGameBoardsUpdate")
                .add("playerID", player.getId())
                .add("gameID", game.getId())
                .add("gameStart", game.getGameStart().toString())
                .add("locked", game.isGameLocked())
                .add("newBoard", game.getFieldGrid().getBoardByPlayerID(player.getId()).toJson())
                .build();
        return joinGameJS;
    }

    private JsonObject createLeaveGameMessage(GameRoom game, PlayerData player) {
        JsonProvider provider = JsonProvider.provider();
        JsonObjectBuilder leaveGameMessage = provider.createObjectBuilder();

        JsonObject leaveGameJS = leaveGameMessage
                .add("action", "leaveGame")
                .add("leftPlayerID", player.getId())
                .add("gameID", game.getId())
                .add("host", game.getHost().getId())
                .build();
        return leaveGameJS;

    }

    private void sendToAllConnectedSessions(JsonStructure message) {
        sessions.entrySet().forEach((session) -> {
            sendToSession(session.getKey(), message);
        });
    }

    private void sendToAllInGameSessions(GameRoom game, JsonStructure message) {
        game.getFieldGrid().getBoards().stream().map((board) -> board.getPlayer()).forEachOrdered((joinedPlayer) -> {
            sessions.forEach((thisSession, playerData) -> {
                if (playerData.equals(joinedPlayer)) {
                    sendToSession(thisSession, message);
                }
            });
        });
    }

    private void sendToSession(Session session, JsonStructure message) {
        try {
            JsonProvider provider = JsonProvider.provider();

            JsonObject timeAdd = provider.createObjectBuilder()
                    .add("message", message)
                    .add("appTime", LocalDateTime.now().toString())
                    .build();

            session.getBasicRemote().sendText(timeAdd.toString());
        } catch (IOException ex) {
            sessions.remove(session);
            Logger.getLogger(GameSessionHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
