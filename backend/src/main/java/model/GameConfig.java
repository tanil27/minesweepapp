/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.math.BigDecimal;
import javax.json.JsonObject;
import javax.json.spi.JsonProvider;

/**
 *
 * @author tanil
 */
public class GameConfig {
    int playerAmount;
    int playerLivesAmt;
    int customMineAmt; 
    int brdWidth; 
    int brdHeight;

    public GameConfig(int playerAmount, int playerLivesAmt, int customMineAmt, int brdWidth, int brdHeight) {
        this.playerAmount = playerAmount;
        this.playerLivesAmt = playerLivesAmt;
        this.customMineAmt = customMineAmt; 
        this.brdWidth = brdWidth; 
        this.brdHeight = brdHeight;
    }
    
    public int getPlayerAmount() {
        return playerAmount;
    }

    public void setPlayerAmount(int playerAmount) {
        this.playerAmount = playerAmount;
    }

    public int getPlayerLivesAmt() {
        return playerLivesAmt;
    }

    public void setPlayerLivesAmt(int playerLivesAmt) {
        this.playerLivesAmt = playerLivesAmt;
    }

    public int getCustomMineAmt() {
        return customMineAmt;
    }

    public void setCustomMineAmt(int customMineAmt) {
        this.customMineAmt = customMineAmt;
    }

    public int getBrdWidth() {
        return brdWidth;
    }

    public void setBrdWidth(int brdWidth) {
        this.brdWidth = brdWidth;
    }

    public int getBrdHeight() {
        return brdHeight;
    }

    public void setBrdHeight(int brdHeight) {
        this.brdHeight = brdHeight;
    }

    public JsonObject toJson() {
        JsonProvider provider = JsonProvider.provider();
        JsonObject configJS = provider.createObjectBuilder()
                .add("playerAmount", this.playerAmount)
                .add("playerLivesAmt", this.playerLivesAmt)
                .add("customMineAmt", this.customMineAmt)
                .add("brdWidth", this.brdWidth)
                .add("brdHeight", this.brdHeight)
                .build();
        return configJS;
    }

}
