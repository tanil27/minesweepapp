/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.spi.JsonProvider;

/**
 *
 * @author tanil
 */
public class FieldGrid {

    private int id;
    private ArrayList<ArrayList<CellData>> grid = new ArrayList<>(0);
    private ArrayList<ArrayList<CellData>> groups = new ArrayList<>(0);
    private ArrayList<CellData> mineList = new ArrayList<>(0);
    private ArrayList<BoardData> boards = new ArrayList<>(0);
    private int width;
    private int height;
    private int mineCount;
    private int activeMineCount;
    private int flagUsed;
    private int spacesLeftToReveal;

    public FieldGrid() {
        
    }

    public FieldGrid(ArrayList<ArrayList<CellData>> _fgd, int width, int height, int mineCount) {
        this.grid = _fgd;
        this.width = width;
        this.height = height;
        this.mineCount = mineCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<ArrayList<CellData>> getGrid() {
        return grid;
    }

    public void setGrid(ArrayList<ArrayList<CellData>> grid) {
        this.grid = grid;
    }

    public ArrayList<ArrayList<CellData>> getGroups() {
        return groups;
    }

    public void setGroups(ArrayList<ArrayList<CellData>> groups) {
        this.groups = groups;
    }

    public void resetGroups() {
        this.groups.clear();
    }

    public void copyGroupOver(int copyFrom, int copyTo) {
        ArrayList<CellData> copy = this.groups.get(copyFrom);
        
        copy.forEach(copyCell -> {
            copyCell.setGroup(copyTo);
        });

        this.groups.get(copyTo).addAll(copy);
        this.groups.get(copyFrom).clear();
        //this.groups.remove(copyFrom);                                
    }

    public void setCellsGroup(int x, int y, int group) {
        CellData cell = this.grid.get(y).get(x);
        if (this.groups.size() <= group) {
            ArrayList<CellData> arlist = new ArrayList<>();
            arlist.add(cell);
            this.groups.add(group, arlist);
        } else {
            this.groups.get(group).add(cell);
        }
        cell.setGroup(group);
    }

    public ArrayList<CellData> getGroupByLetter(int letter) {
        return this.groups.get(letter);
    }

    public ArrayList<CellData> getMineList() {
        return mineList;
    }

    public BoardData getBoardByPlayerID(String playerID) {
        for(BoardData board : this.boards) {
            if (board.getPlayer().getId() == null ? playerID == null : board.getPlayer().getId().equals(playerID)) {
                return board;
            }
        }
        return null;
    }
    
    public BoardData getBoard(int id) {
        return this.boards.get(id);
    }

    public void addBoard(BoardData board) {
        this.boards.add(board);
    }

    public PlayerData getPlayer(String playerID) {
        for (BoardData board : this.boards) {
            if (board.getPlayer().getId() == null ? playerID == null : board.getPlayer().getId().equals(playerID)) {
                return board.getPlayer();
            }
        }
        return null;
    }

    public Boolean removePlayer(String playerID) {
        for (BoardData board : this.boards) {
            if (board.getPlayer().getId() == playerID) {
                Boolean didRemove = this.boards.remove(board);
                if(didRemove) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    public void setMineList(ArrayList<CellData> mineList) {
        this.mineList = mineList;
    }

    public void resetMineList() {
        this.mineList.clear();
    }

    public CellData getCell(int x, int y) {
        return this.grid.get(y).get(x);
    }

    public ArrayList<BoardData> getBoards() {
        return boards;
    }

    public void setBoards(ArrayList<BoardData> boards) {
        this.boards = boards;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getMineCount() {
        return mineCount;
    }

    public void setMineCount(int mineCount) {
        this.mineCount = mineCount;
    }

    public int getActiveMineCount() {
        return activeMineCount;
    }

    public void updateActiveMineCount(int used) {
        this.activeMineCount = activeMineCount + used;
    }

    public void resetActiveMineCount() {
        this.activeMineCount = this.mineCount;
    }

    public int getFlagUsed() {
        return flagUsed;
    }

    public void setFlagUsed(int flagUsed) {
        this.flagUsed = flagUsed;
    }

    public void updateFlagUsed(int used) {
        this.flagUsed = this.flagUsed + used;
    }

    public void resetFlagUsed() {
        this.flagUsed = this.mineCount;
    }

    public int getSpacesLeftToReveal() {
        return spacesLeftToReveal;
    }

    public void setSpacesLeftToReveal(int spacesLeftToReveal) {
        this.spacesLeftToReveal = spacesLeftToReveal;
    }

    public void updateSpacesLeftToReveal(int spacesLeft) {
        this.spacesLeftToReveal = this.spacesLeftToReveal + spacesLeft;
    }

    public void resetSpacesLeftToReveal() {
        this.spacesLeftToReveal = this.height * this.width;
    }
    
    public void removeBoard(int id) {
        //memory reallocation algorithm needed for optimisation
        this.boards.remove(id);
    }

    public JsonObject toJson() {
        JsonProvider provider = JsonProvider.provider();

        JsonArrayBuilder colGridJsBuilder = provider.createArrayBuilder();
        for (ArrayList<CellData> cellGridCol : this.grid) {
            JsonArrayBuilder rowJsBuilder = provider.createArrayBuilder();
            for (CellData cell : cellGridCol) {
                rowJsBuilder.add(cell.toJson());
            }
            JsonArray rowJs = rowJsBuilder.build();
            colGridJsBuilder.add(rowJs);
        }
        JsonArray gridJS = colGridJsBuilder.build();

        JsonArrayBuilder colGroupsJsBuilder = provider.createArrayBuilder();
        for (ArrayList<CellData> cellGridCol : this.groups) {
            JsonArrayBuilder rowJsBuilder = provider.createArrayBuilder();
            for (CellData cell : cellGridCol) {
                rowJsBuilder.add(cell.toJson());
            }
            JsonArray rowJs = rowJsBuilder.build();
            colGroupsJsBuilder.add(rowJs);
        }
        JsonArray groupsJS = colGroupsJsBuilder.build();

        JsonArrayBuilder mineListJsBuilder = provider.createArrayBuilder();
        for (CellData cell : this.mineList) {
            mineListJsBuilder.add(cell.toJson());
        }
        JsonArray mineListJS = mineListJsBuilder.build();

        JsonArrayBuilder boardsJsBuilder = provider.createArrayBuilder();
        for (BoardData board : this.boards) {
            boardsJsBuilder.add(board.toJson());
        }
        JsonArray boardsJS = boardsJsBuilder.build();

        JsonObject fieldGridJs = provider.createObjectBuilder()
                .add("id", this.id)
                //.add("grid", gridJS)
                //.add("groups", groupsJS)
                //.add("mineList", mineListJS)
                .add("boards", boardsJS)
                .add("width", this.width)
                .add("height", this.height)
                .add("mineCount", this.mineCount)
                .add("activeMineCount", this.activeMineCount)
                .add("flagUsed", this.flagUsed)
                .add("spacesLeftToReveal", this.spacesLeftToReveal)
                .build();
        return fieldGridJs;
    }
}
