/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.json.JsonObject;
import javax.json.spi.JsonProvider;

/**
 *
 * @author tanil
 */
public class BoardData {
    private int id;
    private PlayerData player;
    private int flagAmount = 0;
    private int score = 0;
    private int position;
    private int lives;
    private int hits;

    public BoardData(int id, PlayerData player){
        this.id = id;
        this.player = player;
    }
    
    public int getId(){
        return id;
    }
    
    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score += score;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
        this.hits = lives;
    }
    
    public Boolean isAlive() {
        return this.hits > 0;
    }

    public int getHits() {
        return hits;
    }

    public void setHit() {
        this.hits--;
    }
    
    public PlayerData getPlayer() {
        return this.player;
    }
    
    public int getFlagAmount() {
        return this.flagAmount;
    }
    
    public void setFlagAmount(int flagAmount) {
        this.flagAmount = flagAmount;
    }
    
    public void resetBoard() {
        this.hits = this.lives;
        this.score = 0;
        this.position = -1;
    }
    
    public JsonObject toJson() {
        JsonProvider provider = JsonProvider.provider();
        JsonObject configJS = provider.createObjectBuilder()
                .add("id", this.id)
                .add("player", this.player.toJson())
                .add("flagAmount", this.flagAmount)
                .add("score", this.score)
                .add("position", this.position)
                .add("lives", this.lives)
                .add("hits", this.hits)
                .build();
        return configJS;
    }
}
