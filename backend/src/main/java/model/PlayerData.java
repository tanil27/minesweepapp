/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.json.JsonObject;
import javax.json.spi.JsonProvider;

/**
 *
 * @author tanil
 */
public class PlayerData {
    private String id;
    private String username;
    private String firstName;
    private String surname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
    
    public JsonObject toJson() {
        JsonProvider provider = JsonProvider.provider();
        JsonObject configJS = provider.createObjectBuilder()
                .add("id", this.id)
                .build();
        return configJS;
    }
}
