/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import constants.FIELD_STATES;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.json.spi.JsonProvider;

/**
 *
 * @author tanil
 */
public class CellData {

    private int id;
    private int x;
    private int y;
    private int state;
    private boolean checked;
    private BoardData owner;
    private int group;

    public CellData() {
    }

    public CellData(int id, int x, int y) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.state = 0;
        this.checked = false;
        this.owner = null;
        this.group = -1;
    }

    public void setGroup(int group) {
        this.group = (group);
    }

    public int getGroup() {
        return this.group;
    }

    public boolean hasGroup() {
        boolean bool = false;
        if (this.group >= 0) {
            bool = true;
        }
        return bool;
    }

    public void setMine() {
        this.state = FIELD_STATES.MINE;
    }

    public boolean isMine() {
        return this.state == FIELD_STATES.MINE;
    }

    public void setEmpty() {
        this.state = FIELD_STATES.EMPTY;
    }

    public boolean isEmpty() {
        return this.state == FIELD_STATES.EMPTY;
    }

    public boolean isNumber() {
        return this.state >= FIELD_STATES.NUMBER;
    }

    public void setNumber() {
        this.state = FIELD_STATES.NUMBER;
    }

    public void updateNumber() {
        this.state = this.state + 1;
    }

    public boolean isChecked() {
        return this.checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isFlagged() {
        
        return this.owner != null;
    }

    public void setFlag(BoardData owner) {
        this.owner = owner;
    }

    public boolean updateFlag(BoardData owner) {
        this.owner = owner;
        return isFlagged();
    }

    public BoardData getOwner() {
        return this.owner;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public int getState() {
        return this.state;
    }

    public void resetCell() {
        this.state = 0;
        this.checked = false;
        this.owner = null;
        this.group = -1;
    }

    public JsonObject toJson() {
        JsonProvider provider = JsonProvider.provider();
        JsonObjectBuilder cellDataJSbuilder = provider.createObjectBuilder();
        cellDataJSbuilder.add("id", this.id);
        cellDataJSbuilder.add("x", this.x);
        cellDataJSbuilder.add("y", this.y);
        cellDataJSbuilder.add("state", this.state);
        cellDataJSbuilder.add("checked", this.checked);
        cellDataJSbuilder.add("group", this.group);
        
        if (this.owner != null) {
            cellDataJSbuilder.add("owner", this.owner.toJson());
        } else {

            cellDataJSbuilder.add("owner", JsonValue.NULL);
        }

        return cellDataJSbuilder.build();
    }

}
