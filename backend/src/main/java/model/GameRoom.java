/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import constants.PHASE_STATES;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.json.JsonObject;
import javax.json.spi.JsonProvider;
import tasks.PregamePhaseTask;
import tasks.WinGamePhaseTask;

/**
 *
 * @author tanil
 */
public class GameRoom {

    private int id;
    private FieldGrid fieldGrid;
    private LocalDateTime timeCreated;
    private LocalDateTime gameStart;
    private boolean gameLocked = false;
    private String phase = PHASE_STATES.PREGAME;
    private PregamePhaseTask pregamePhase;
    private WinGamePhaseTask winGamePhase;
    private GameConfig config;
    private PlayerData host;
    private int boardIDcount = 0;
    private ArrayList<Integer> leftBoardsID = new ArrayList<>(0);

    public GameRoom(PlayerData player) {
        this.host = player;
    }
    
    public GameConfig getConfig() {
        return config;
    }
    
    public void setPhase(String phase){ 
        this.phase = phase;
    }
    
    public String getPhase() {
        return this.phase;
    }
    
    public boolean isPreGamePhase(){
        return this.phase.equals(PHASE_STATES.PREGAME);
    }
    public boolean isInGamePhase(){
        return this.phase.equals(PHASE_STATES.INGAME);
    }
    public boolean isWinGamePhase(){
        return this.phase.equals(PHASE_STATES.WIN);
    }

    public void startPregamePhase() {
        System.out.println("pregame Phase starting");
        int delay = 5 * 1000;
        setPhase(PHASE_STATES.PREGAME);
        PregamePhaseTask task = new PregamePhaseTask(this, delay);
        this.pregamePhase = task;
    }
            
    public PregamePhaseTask getPregamePhase() {
        return pregamePhase;
    }

    public void cancelPregamePhase() {  
        setPhase(PHASE_STATES.INGAME);
        this.pregamePhase.cancelPregamePhase();
    }
    
    public void startWinGamePhase() {
        int delay = 5 * 1000;
        setPhase(PHASE_STATES.WIN);
        WinGamePhaseTask task = new WinGamePhaseTask(this, delay);
        this.winGamePhase = task;
    }
        
    public WinGamePhaseTask getWinGamePhase() {
        return winGamePhase;
    }
    
    public void cancelWinGamePhase() {
        reset();
        setPhase(PHASE_STATES.PREGAME);
        startPregamePhase();
        this.winGamePhase.cancelWinGamePhase();
        this.winGamePhase = null;
    }
    
    public PlayerData getHost() {
        return host;
    }

    public void setHost(PlayerData host) {
        this.host = host;
    }

    public void setConfig(GameConfig config) {
        this.config = config;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public FieldGrid getFieldGrid() {
        return fieldGrid;
    }

    public void setFieldGrid(FieldGrid fieldGrid) {
        this.fieldGrid = fieldGrid;
    }

    public LocalDateTime getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(LocalDateTime timeCreated) {
        this.timeCreated = timeCreated;
    }

    public LocalDateTime getGameStart() {
        return gameStart;
    }

    public void setGameStart(LocalDateTime gameStart) {
        this.gameStart = gameStart;
    }

    public boolean isGameLocked() {
        return gameLocked;
    }

    public void setGameLocked(boolean gameLocked) {
        this.gameLocked = gameLocked;
    }

    public void setupGame(GameConfig config) {
        setTimeCreated(LocalDateTime.now());
        setConfig(config);
        createMasterBoard(config.getCustomMineAmt(), config.getBrdWidth(), config.getBrdHeight());
        reset();
        startPregamePhase();
    }

    private void createMasterBoard(int customMineAmt, int brdWidth, int brdHeight) {
        int i = 0;
        ArrayList<ArrayList<CellData>> fg = new ArrayList<>();
        for (int y = 0; y < brdHeight; y++) {
            ArrayList<CellData> row = new ArrayList<>();
            for (int x = 0; x < brdWidth; x++) {
                CellData cd = new CellData(i, x, y);
                row.add(cd);
                i++;
            }
            fg.add(row);
        }
        this.fieldGrid = new FieldGrid(fg, brdWidth, brdHeight, customMineAmt);
    }

    public boolean addPlayer(PlayerData player) {
        if (this.fieldGrid.getBoards().size() == this.config.getPlayerAmount()) {
            System.out.println("MAX PLAYERS");
            return false;
        }

        if (!this.fieldGrid.getBoards().stream().noneMatch((board) -> (board.getPlayer() == player))) {
            System.out.println("ALREADY JOINED");
            return false;
        }
        if (this.isGameLocked()) {
            System.out.println("GAMELOCKED");
            return false;
        }
        
        int useID = boardIDcount;
        Boolean updateBoardCount = true;
        if(!leftBoardsID.isEmpty()) {
            updateBoardCount = false;
            useID = leftBoardsID.get(0);
            leftBoardsID.remove(0);
        }
        
        BoardData board = new BoardData(useID, player);
        board.setLives(this.config.getPlayerLivesAmt());
        this.fieldGrid.addBoard(board);
        
        if(updateBoardCount) {
            boardIDcount++;
        }
        
        
        if (this.fieldGrid.getBoards().size() == this.config.getPlayerAmount()) {
            setGameLocked(true);
        }
        return true;
    }
    
    public Boolean removePlayer(String playerID) {
        int boardID = this.fieldGrid.getBoardByPlayerID(playerID).getId();
        leftBoardsID.add(boardID);
        return this.fieldGrid.removePlayer(playerID);
    }
    
    public void reset() {
        System.out.println("RESETTING GAME["+this.id+"]");
        this.fieldGrid.resetFlagUsed();
        this.fieldGrid.resetActiveMineCount();
        this.fieldGrid.resetSpacesLeftToReveal();

        for (int i = 0; i < this.fieldGrid.getBoards().size(); i++) {
            BoardData board = this.fieldGrid.getBoard(i);
            board.resetBoard();
            board.setFlagAmount(0);
        }

        resetCells();
        setMines();
        setHintNumbers();
        setEmptySpaces();
    }

    private void resetCells() {
        ArrayList<ArrayList<CellData>> grid = this.fieldGrid.getGrid();

        grid.forEach((row)
                -> {
            row.forEach((cell) -> {
                cell.resetCell();
            });
        });
    }

    private class Pair {

        int x;
        int y;

        private Pair(int x, int y) {
            this.x = x;
            this.y = y;
        }

        private int getX() {
            return x;
        }

        private int getY() {
            return y;
        }
    }

    private Set<Pair> generateRandomMines() {

        Set<Pair> tempHash = new HashSet<>();

        Random rand = new Random();

        int loaded = 0;
        int fgWidth = this.fieldGrid.getWidth();
        int fgHeight = this.fieldGrid.getHeight();
        int fgMineCount = this.config.getCustomMineAmt();

        int posIndexX, posIndexY;
        
        Pair p;

        while (loaded < fgMineCount) {
            posIndexX = rand.nextInt(fgWidth);
            posIndexY = rand.nextInt(fgHeight);

            p = new Pair(posIndexX, posIndexY);

            if (!tempHash.contains(p)) {
                tempHash.add(p);
                loaded++;
            }
        }

        return tempHash;
    }

    private void setMines() {
        Set<Pair> randomMines = generateRandomMines();

        int xPos;
        int yPos;
        ArrayList<ArrayList<CellData>> grid = this.fieldGrid.getGrid();
        this.fieldGrid.resetMineList();
        ArrayList<CellData> mineList = new ArrayList<>(this.config.getCustomMineAmt());

        for (Pair pair : randomMines) {
            xPos = pair.getX();
            yPos = pair.getY();

            CellData cell = grid.get(yPos).get(xPos);
            cell.setMine();
            mineList.add(cell);
        }
        this.fieldGrid.setMineList(mineList);
    }

    private void setHintNumbers() {
        int xPos, yPos;
        ArrayList<CellData> mineList = this.fieldGrid.getMineList();

        int[][] positions = {{-1, 0}, {-1, -1}, {0, -1}, {1, -1}, {1, 0}, {1, 1}, {0, 1}, {-1, 1}};

        for (CellData cell : mineList) {
            for (int[] position : positions) {
                xPos = cell.getX();
                yPos = cell.getY();
                checkConnections(xPos + position[0], yPos + position[1]);
            }
        }

    }

    private void checkConnections(int x, int y) {
        if (isCellInBounds(x, y)) {
            CellData cell = this.fieldGrid.getGrid().get(y).get(x);
            if (!cell.isMine()) {
                cell.updateNumber();
            }
        }
    }

    private boolean isCellInBounds(int x, int y) {
        if (x < 0) {
            return false;
        }
        if (y < 0) {
            return false;
        }
        if (x > this.fieldGrid.getWidth() - 1) {
            return false;
        }
        return y <= this.fieldGrid.getHeight() - 1;
    }

    private class groupCell {

        boolean group;
        CellData cell;

        private groupCell(boolean group, CellData cell) {
            this.group = group;
            this.cell = cell;
        }
    }

    private class setNumberObject {

        groupCell top, left, right, bottom, center;

        private setNumberObject() {
            top = new groupCell(false, null);
            left = new groupCell(false, null);
            right = new groupCell(false, null);
            bottom = new groupCell(false, null);
            center = new groupCell(true, null);
        }

        private ArrayList<groupCell> getCellDirections() {
            ArrayList<groupCell> gc = new ArrayList<>();
            gc.add(top);
            gc.add(left);
            gc.add(right);
            gc.add(bottom);
            gc.add(center);
            return gc;
        }

    }

    private void setEmptySpaces() {

        CellData cell, topCell, leftCell, rightCell, bottomCell;
        int _x, _y, x_, y_;
        ArrayList<Integer> changedGroups = new ArrayList<>();
        this.fieldGrid.resetGroups();

        setNumberObject setNumberObj;

        for (int y = 0; y < this.fieldGrid.getHeight(); y++) {
            for (int x = 0; x < this.fieldGrid.getWidth(); x++) {

                cell = this.fieldGrid.getCell(x, y);

                setNumberObj = new setNumberObject();

                setNumberObj.center.cell = cell;
                
                topCell = null;
                leftCell = null;
                rightCell = null;
                bottomCell = null;

                if (!cell.isMine() && !cell.isNumber()) {

                    _x = x - 1;
                    _y = y - 1;
                    x_ = x + 1;
                    y_ = y + 1;

                    changedGroups.clear();

                    int tempGroup;
                    if (this.fieldGrid.getGroups() == null) {
                        tempGroup = 0;
                    } else {
                        tempGroup = this.fieldGrid.getGroups().size();
                    }

                    if (isCellInBounds(x, _y)) {
                        topCell = this.fieldGrid.getCell(x, _y);
                        if (topCell.isEmpty() && topCell.hasGroup()) {
                            if (topCell.getGroup() > tempGroup) {
                            } else {
                                tempGroup = topCell.getGroup();
                            }
                            changedGroups.add(topCell.getGroup());
                        }
                        if (topCell.isNumber()) {
                            setNumberObj.top.group = true;
                            setNumberObj.top.cell = topCell;
                        }
                    }
                    if (this.isCellInBounds(_x, y)) {
                        //neighbour cell: left
                        leftCell = this.fieldGrid.getCell(_x, y);
                        if (leftCell.isEmpty() && leftCell.hasGroup()) {
                            if (leftCell.getGroup() > tempGroup) {
                            } else {
                                tempGroup = leftCell.getGroup();
                            }
                            changedGroups.add(leftCell.getGroup());
                        }
                        if (leftCell.isNumber()) {
                            setNumberObj.left.group = true;
                            setNumberObj.left.cell = leftCell;
                        }
                    }

                    if (this.isCellInBounds(x_, y)) {
                        //neighbour cell: right
                        rightCell = this.fieldGrid.getCell(x_, y);
                        if (rightCell.isEmpty() && rightCell.hasGroup()) {
                            if (rightCell.getGroup() > tempGroup) {
                            } else {
                                tempGroup = rightCell.getGroup();
                            }
                            changedGroups.add(rightCell.getGroup());
                        }
                        if (rightCell.isNumber()) {
                            setNumberObj.right.group = true;
                            setNumberObj.right.cell = rightCell;
                        }
                    }
                    if (this.isCellInBounds(x, y_)) {
                        //neighbour cell: bottom
                        bottomCell = this.fieldGrid.getCell(x, y_);
                        if (bottomCell.isEmpty() && bottomCell.hasGroup()) {
                            if (bottomCell.getGroup() > tempGroup) {
                            } else {
                                tempGroup = bottomCell.getGroup();
                            }
                            changedGroups.add(bottomCell.getGroup());
                        }
                        if (bottomCell.isNumber()) {
                            setNumberObj.bottom.group = true;
                            setNumberObj.bottom.cell = bottomCell;
                        }
                    }

                    //check if it should be grouped
                    for (groupCell gc : setNumberObj.getCellDirections()) {
                        if (gc.group) {
                            this.fieldGrid.setCellsGroup(gc.cell.getX(), gc.cell.getY(), tempGroup);
                        }
                    }
                    
                    for (Integer cg : changedGroups) {
                        if (!cg.equals(tempGroup)) {
                            this.fieldGrid.copyGroupOver(cg, tempGroup);
                        }
                    }
                }
            }
        }
        System.out.println("SETTING EMPTY SPACES: "+ this.fieldGrid.getGroups().size());
    }

    /**
     * 
     * @param x
     * @param y
     * @param mouseButton
     * @param boardData
     * @return ArrayList<CellData>
     */
    public /*ArrayList<CellData>*/ void mouseUpEventHandler(int x, int y, int mouseButton, BoardData boardData) {
        //ArrayList<CellData> affectedCells = new ArrayList<>();
        if (isInGamePhase()) {
            if (boardData.isAlive()) {
                if (isCellInBounds(x, y)) {
                    CellData cell = this.fieldGrid.getCell(x, y);
                    if (mouseButton == 0) {
                        if (!cell.isFlagged() && !cell.isChecked()) {
                            if (cell.isMine()) {
                                cell.setChecked(true);
                                //affectedCells.add(cell);
                                //FieldGrid.updateFlagUsed(-1);
                                this.fieldGrid.updateActiveMineCount(-1);
                                this.fieldGrid.updateSpacesLeftToReveal(-1);
                                setBoardDeath(boardData);

                                if (isLastManStanding()) {
                                    ArrayList<CellData> revealedCells = doGameOver();
                                    //affectedCells.addAll(revealedCells);
                                }
                            } else if (cell.isNumber()) {
                                //affectedCells.add(cell);
                                cell.setChecked(true);
                                this.fieldGrid.updateSpacesLeftToReveal(-1);
                            } else {                              
                                ArrayList<CellData> grouping = this.fieldGrid.getGroupByLetter(cell.getGroup());
                                for (int i = 0; i < grouping.size(); i++) {
                                    if (!grouping.get(i).isFlagged() && !grouping.get(i).isChecked()) {
                                        this.fieldGrid.updateSpacesLeftToReveal(-1);
                                        grouping.get(i).setChecked(true);
                                        //affectedCells = grouping;
                                    }
                                }
                            }
                        }
                    } else {
                        BoardData cplayer = boardData;
                        if (!cell.isChecked()) {
                            if (cell.getOwner() == null || cell.getOwner() == cplayer) {
                                if (!cell.isFlagged()) {
                                    cell.updateFlag(cplayer);
                                    this.fieldGrid.updateFlagUsed(-1);
                                    this.fieldGrid.updateSpacesLeftToReveal(-1);
                                    boardData.setFlagAmount(boardData.getFlagAmount() + 1);
                                } else {
                                    cell.updateFlag(null);
                                    this.fieldGrid.updateFlagUsed(1);
                                    this.fieldGrid.updateSpacesLeftToReveal(1);
                                    boardData.setFlagAmount(boardData.getFlagAmount() - 1);
                                }
                            }
                            //affectedCells.add(cell);
                        }
                    }
                    if(this.fieldGrid.getSpacesLeftToReveal() == 0) {
                        System.out.println("Space Left to reveal : " + 0);
                        doGameOver();
                    }
                } else {
                    System.out.println("mouse clicked failed: cell out of bound");
                }
            } else {
                System.out.println("mouse clicked failed: player dead");
            }
        } else {
            System.out.println("mouse clicked failed: game locked");
        }
        //return affectedCells;
    }

    private void setBoardDeath(BoardData boardData){
        boardData.setHit();
        boardData.setScore(-this.fieldGrid.getMineCount());
    }
    
    private boolean isLastManStanding(){
        int maxDeath = this.fieldGrid.getBoards().size();
        int deathCount = 0;
        if(maxDeath == 1) {
            //1 player game
            if(this.fieldGrid.getBoard(0).isAlive()) {
                return false;
            }
            return true;
        }
        for(int i = 0; i < maxDeath; i++) {
            if(!this.fieldGrid.getBoard(i).isAlive()) {
                deathCount++;
            }
            if(deathCount == maxDeath - 1) {
                return true;
            }
        }
        return false;
    }
    
    private ArrayList<CellData> doGameOver() {
        this.gameLocked = true;
        ArrayList<CellData> affectedCells = revealField();
        getWinner();
        
        //Win Phase
        startWinGamePhase();
        return affectedCells;
    }
    
    private ArrayList<CellData> revealField() {
        ArrayList<CellData> affectedCells = new ArrayList<>();
        for (int y = 0; y < this.fieldGrid.getWidth(); y++) {            
            for (int x = 0; x < this.fieldGrid.getHeight(); x++) {
                CellData cell = this.fieldGrid.getCell(x, y);
                
                if(!cell.isFlagged() && !cell.isChecked()) {
                    cell.setChecked(true);
                }
                
                if(cell.isFlagged()) {
                    if(cell.getOwner().isAlive()) {
                        if(cell.isMine()) {
                            cell.getOwner().setScore(1);
                        } else {
                            cell.getOwner().setScore(-1);
                        }
                    }
                }
                affectedCells.add(cell);
            }
        }
        return affectedCells;
    }
    
    private void getWinner() {
        Map<Integer, ArrayList<BoardData>> groupList = new HashMap<>();
        
        this.fieldGrid.getBoards().forEach(board -> {
            if(!groupList.containsKey(board.getScore())){
                ArrayList<BoardData> newArr = new ArrayList<>();
                newArr.add(board);
                groupList.put(board.getScore(), newArr);
            } else {
                groupList.get(board.getScore()).add(board);
            }
        });
        
        SortedSet<Integer> keys = new TreeSet<>(groupList.keySet());
        
        int pos = groupList.size() - 1;
        for(int key : keys) {
            ArrayList<BoardData> group = groupList.get(key);
            for(BoardData board : group) {
                setPlace(board, pos);
            }
            pos--;
        }
    }
    
    private void setPlace(BoardData board, int pos) {
        board.setPosition(pos);
    }
    
    public JsonObject toJson() {
        JsonProvider provider = JsonProvider.provider();
        JsonObject gameJS = provider.createObjectBuilder()
                .add("id", this.id)
                .add("fieldGrid", this.fieldGrid.toJson())
                .add("timeCreated", this.timeCreated.toString())
                .add("gameStart", this.gameStart.toString())
                .add("phase", this.phase)
                .add("gameLocked", this.gameLocked)
                .add("config", this.config.toJson())
                .add("host", this.host.getId())
                .build();
        return gameJS;
    }

}